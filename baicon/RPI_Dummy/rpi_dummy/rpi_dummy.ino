// Wire Master Reader
// by Nicholas Zambetti <http://www.zambetti.com>

// Demonstrates use of the Wire library
// Reads data from an I2C/TWI slave device
// Refer to the "Wire Slave Sender" example for use with this

// Created 29 March 2006

// This example code is in the public domain.

#include <Wire.h>

#define updateRQ  2
#define stateMonitor  3
#define buckDummy 4
#define TASK_ROUT 4

unsigned char package[12];
int byteCount = 0;

typedef enum {
  POWER_ON,
  INTERFACE,
  SWITCH_OFF,
} RPI_State;

typedef enum {
  BUTTON = 0x01,
  ENCODER,
  KNOBS,
  SHUTDOWN = 0xFF,
}I2C_Source;

unsigned char rpiState = POWER_ON;

void setup()
{
  Wire.begin();        // join i2c bus (address optional for master)
  pinMode(updateRQ, INPUT);
  pinMode(buckDummy, INPUT);
  pinMode(stateMonitor, OUTPUT);
  digitalWrite(stateMonitor, LOW);
  Serial.begin(9600);  // start serial for output
}

void loop()
{

  switch (rpiState) {

    case POWER_ON:
      powerOnProcedure();
      rpiState = INTERFACE;
      Serial.println(">> INTERFACE_MODE entered");  
      break;

    case INTERFACE:
      if (digitalRead(2) == LOW) {
        Wire.requestFrom(0x12, 12);    // request 6 bytes from slave device #2
        while (Wire.available())    // slave may send less than requested
        {
          package[byteCount++] = Wire.read(); // receive a byte as character
        }
        byteCount = 0;
        displayPackage();
        if(package[11] == 0xFF){
          Serial.println(">> SHUTDOWN_CMD detected");
          rpiState = SWITCH_OFF;  
        }
      }

      break;

    case SWITCH_OFF:
      powerOffProcedure();
      rpiState = POWER_ON;
    break;
  }
}

void powerOffProcedure(void){
  Serial.print("   --Simulate Stop Threads");  
  for(int i = 0; i < TASK_ROUT; i++){
    delay(2000);
    Serial.print(".");
  }
  Serial.println("  --done");         
  digitalWrite(stateMonitor, LOW);   
  Serial.println("   --Shutdown Simulation COMPLETED");  
  Serial.print(">> Wait for BUCK Power Off");
  while(digitalRead(buckDummy) == HIGH);
  Serial.println("  --done");         
  Serial.println(">> SESSION COMPLETED!");
  Serial.println(">> Prepare for POWERON_MODE");
}

void powerOnProcedure(void){
  Serial.print(">> Waiting for BUCK");         
  while(digitalRead(buckDummy) == LOW);
  Serial.println("  --done");         
  Serial.print(">> Simulating Power On Time");
  for(int i = 0; i < TASK_ROUT; i++){
    delay(1000);
    Serial.print(".");

  }
  Serial.println("  --done");  
  digitalWrite(stateMonitor, HIGH);       
  Serial.println(">> Power State sent to BAICON");
}

void displayPackage(void) {
  int start = ((package[0] << 8) + package[1]) & 0xFFFF;
  int buttons = package[3] & 0xFF;
  int encoder = package[4] & 0xFF;
  int knob1 = ((package[5] << 8) + package[6]) & 0x0FFF;
  int knob2 = ((package[7] << 8) + package[8]) & 0x0FFF;
  int knob3 = ((package[9] << 8) + package[10]) & 0x0FFF;
  String str;
  switch (package[11]) {
    case BUTTON:
      str = "BUTTON";
      break;

    case ENCODER:
      str = "ENCODER";
      break;

    case KNOBS:
      str = "KNOBS";
      break;

    case SHUTDOWN:
      str = "SHUIDOWN";
      break;

    default:
      str = package[11];
      break;
  }

  Serial.print("   Start:   ");         // print the character
  Serial.println(start, HEX);           // print the character
  Serial.print("   Button:  ");         // print the character
  Serial.println(buttons, BIN);         // print the character
  Serial.print("   Encoder: ");         // print the character
  Serial.println(encoder);              // print the character
  Serial.print("   Knob1:   ");         // print the character
  Serial.println(knob1);                // print the character
  Serial.print("   Knob2:   ");         // print the character
  Serial.println(knob2);                // print the character
  Serial.print("   Knob3:   ");         // print the character
  Serial.println(knob3);                // print the character
  Serial.println("   Source:  " + str);

  Serial.println("======Complete======");
}


