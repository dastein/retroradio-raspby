#include <msp430fr5969.h> 
#include <stdint.h>
#include <stdbool.h>

#define RPI_PM_OFFSET	BIT2
#define PLM_OFFSET		BIT4
#define	BUCK_EN_OFFSET	BIT4

bool powerline_flag = false;
bool pipower_flag = false;

void initGPIO(void){
	//Configure RPI Power Monitor (RPI_PM)
	P1DIR &= ~RPI_PM_OFFSET;	//RPI_PM input

	//Configure Power Line Monitor (PLM)
	P1DIR &= ~PLM_OFFSET;	//PLM input
	P1IES |= PLM_OFFSET;  //Select interrupt on falling edge for power on detection
    P1IFG &= ~PLM_OFFSET; //Clear interrupt flag register

	//Configure Buck Enable (BUCK_EN)
	P3DIR |= BUCK_EN_OFFSET;	//BUCK_EN output
	P3OUT &= ~BUCK_EN_OFFSET;	//BUCK_EN_LOW
}


void _initPwrSys(void){
	initGPIO();
}

/*
 * Summary: Enable all user interface interrupts
 */
inline void _pwr_IE(void){
	P1IE |= PLM_OFFSET;
}

/*
 * Summary: Disable all user interface interrupts
 */
inline void _pwr_ID(void){
    P1IE &= ~PLM_OFFSET;
}

inline void _buck_Enable(){
    P3OUT |= BUCK_EN_OFFSET;
}

inline void _buck_Disable(){
    P3OUT &= ~BUCK_EN_OFFSET;
}

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	//Stop watchdog timer
	PM5CTL0 &= ~LOCKLPM5;       //Unlock LPMx.5 configurations

	P1DIR |= BIT0;
	P1OUT &= ~BIT0;


	_initPwrSys();
	_pwr_IE();

	_EINT();

	while(1){
		if(powerline_flag){
			_buck_Enable();
			pipower_flag = true;
			powerline_flag = false;
		}

		if(P1IN & RPI_PM_OFFSET && pipower_flag){
			P1OUT |= BIT0;
			P1IES 
		}
	}


	return 0;
}


//*******************************
//         UI Event Handler
//*******************************

inline void __pwr_PowerLine_Handler(bool *flag){
    P1IFG &= ~PLM_OFFSET;
    *flag = true;
}

#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{
	if (P1IFG & PLM_OFFSET){
		__pwr_PowerLine_Handler(&powerline_flag);
    }
}
