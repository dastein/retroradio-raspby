/*
 * icom.h
 *
 *  Created on: 18.11.2018
 *      Author: mgeik
 */

#ifndef ICOM_H_
#define ICOM_H_

#include<stdint.h>
#include<stdbool.h>
#include<stdlib.h>

#define PACKAGE_SIZE 12
#define BUNDLE_SIZE 10
#define SLAVE_ADDRESS 0x12
#define SHUTDOWM_SOURCE 0xFF

#define ICOM_TX_TIMEOUT   1       /*In 100 ms*/


typedef enum{
    SCAN,
    TRANSMIT,
}Interface_Process;

//*******************************
//    Libary Support Functions
//*******************************
void initGPIO(void);
void initI2C(void);

//*******************************
//         UI Functions
//*******************************
void _initICOM(void);
extern void _icom_IE(void);
extern void _icom_ID(void);

extern void _startUpdateRQ(void);
extern void _stopUpdateRQ(void);
void _preparePackage(uint8_t* bundle);

//*******************************
//         UI Event Handler
//*******************************
inline void __icom_I2C_Handler(bool* flag);


#endif /* ICOM_H_ */
