#include <msp430fr5969.h>
#include <pwrsys.h>


uint32_t sysCount = 0;
bool reset_flag = false;

//*******************************
//    Libary Support Functions
//*******************************

void initGPIO(void){
	//Configure RPI Power Monitor (RPI_PM)
	P4DIR &= ~RPI_PM_OFFSET;	//RPI_PM input

	//Configure Power Line Monitor (PLM)
	P4DIR &= ~PLM_OFFSET;	//PLM input
	P4IES &= ~PLM_OFFSET;  //Select interrupt on rising edge for power on detection
    P4IFG &= ~PLM_OFFSET; //Clear interrupt flag register

	//Configure Buck Enable (BUCK_EN)
	P4DIR |= BUCK_EN_OFFSET;	//BUCK_EN output
	P4OUT &= ~BUCK_EN_OFFSET;	//BUCK_EN_LOW
}

/*
* Summary: System timer required for timeouts
*/
void initSysTimer(void){
    // Configure Timer1_A
    TA1CCR0 = SYS_PERIOD_COUNT;                 //Conversion Period
    TA1CTL = TASSEL__SMCLK | MC__UP | ID_1;     // SMCLK, UP mode, Clock divide by 2 = 0.1s @CCR = 50000
}

//*******************************
//         UI Functions
//*******************************

void _initPwrSys(void){
	initGPIO();
    initSysTimer();
}

void _prepareShutdown(void){
    P4IES &= ~RPI_PM_OFFSET;  //Prepare RPI_PM to decect rising edge
    P4IE |= RPI_PM_OFFSET;
}


inline void _buck_Enable(){
    P4OUT |= BUCK_EN_OFFSET;
}

inline void _buck_Disable(){
    P4OUT &= ~BUCK_EN_OFFSET;
}

inline bool _getPwrLineMonitor(){
    return (P4IN & PLM_OFFSET);
}

inline bool _getRpiPowerMonitor(){
    return (P4IN & RPI_PM_OFFSET);
}

inline long _getSysCount(){
    return sysCount;
}

/*
* Summary: Reset flag at the next period event
*/
inline void _resetSysCount(){
     reset_flag = true;
     while(reset_flag); //Wait until timer was resetted
}

/*
 * Summary: Disable all user interface interrupts
 */
inline void _pwr_ID(void){
    //P1IE &= ~PLM_OFFSET;
    P4IE &= ~RPI_PM_OFFSET;
    TA1CCTL0 = 0;            // TACCR0 interrupt enabled
}

/*
 * Summary: Enable all user interface interrupts
 */
inline void _pwr_IE(void){
	//P1IE |= PLM_OFFSET;
    P4IE |= RPI_PM_OFFSET;
    TA1CCTL0 = CCIE;            // TACCR0 interrupt disable
}

inline void _enableWakeUpDetection(){
    P4IE |= PLM_OFFSET;
}

inline void _disableWakeUpDetection(){
    P4IE &= ~PLM_OFFSET;
}
//*******************************
//         UI Event Handler
//*******************************

inline void __pwr_PowerLine_Handler(bool *flag){
    P4IFG &= ~PLM_OFFSET;
    *flag = true;
}

inline void __pwr_RPIMonitor_Handler(bool *flag){
    P4IFG &= ~RPI_PM_OFFSET;
    *flag = true;
}

inline void __sys_Timer_Handler(){

    sysCount++;

    if(reset_flag){
        sysCount = 0;
        reset_flag = false;
    }
}


