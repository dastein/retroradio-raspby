/*
 * pwrsys.h
 *
 *  Created on: 20.11.2018
 *      Author: mgeik
 */

#include <stdint.h>
#include <stdbool.h>


#ifndef PWRSYS_H_
#define PWRSYS_H_

#define PWR_BUNDLE_SIZE     10
#define PWR_LOSS          	0xFF

#define RPI_PM_OFFSET	BIT3
#define PLM_OFFSET		BIT7
#define	BUCK_EN_OFFSET	BIT2

#define SYS_PERIOD_COUNT    50000
#define SYS_PWR_HOLD_TIME   5       /*In 100 ms*/
#define SYS_PIBOOT_TIMEOUT  50      /*In 100 ms*/
#define SYS_PISHUTDOWN_TIMEOUT  100      /*In 100 ms*/


typedef enum{
    POWER_TRIGGER,
    PI_BOOT,
    INTERFACE_PREPARATION,
}PowerUp_Process;

typedef enum{
    TRANSPORT,
    PI_SHUTDOWN,
    GO_SLEEP,
}ShutDown_Process;

//*******************************
//    Libary Support Functions
//*******************************

void initGPIO(void);
void initSysTimer(void);

//*******************************
//         UI Functions
//*******************************

void _initPwrSys(void);
void _prepareInterfaceMode(void);
void _prepareShutdown(void);
extern void _buck_Enable();
extern void _buck_Disable();
extern bool _getPwrLineMonitor();
extern bool _getRpiPowerMonitor();
extern long _getSysCount();
extern void _resetSysCount();
extern void _pwr_ID(void);
extern void _pwr_IE(void);
extern void _enableWakeUpDetection();
extern void _disableWakeUpDetection();

//*******************************
//         UI Event Handler
//*******************************

extern void __pwr_PowerLine_Handler(bool *flag);
extern void __pwr_RPIMonitor_Handler(bool *flag);
extern void __sys_Timer_Handler();

#endif /* PWRSYS_H_ */
