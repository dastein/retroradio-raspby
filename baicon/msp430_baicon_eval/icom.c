/*
 * icom.c
 *
 *  Created on: 18.11.2018
 *      Author: mgeik
 */

#include "icom.h"
#include "msp430fr5969.h"

uint8_t txCount = 0;
uint8_t rxBuff = 0;

uint8_t icomPackage[PACKAGE_SIZE];

//*******************************
//    Libary Support Functions
//*******************************

inline void initGPIO(void){
    P1SEL1 |= (BIT6 | BIT7);  //Enable I2C functionality on pins

    P3DIR |= BIT0;  //Set output for update request pin
    P3OUT |= BIT0;  //Update RQ low active
}

inline void initI2C(void){
    UCB0CTLW0 |= UCSWRST;    //Enable SW Reset to disable USCI Module
    UCB0CTLW0 &= ~UCMST;     //Select slave mode
    UCB0CTLW0 |= UCMODE_3 | UCSYNC;  //Select I2C mode, select synchronous mode
    UCB0I2COA0 = UCOAEN | SLAVE_ADDRESS;

    UCB0CTLW0 &= ~UCSWRST;    //Disable SW Reset to enable USCI Module
    UCB0IFG &= ~(UCSTPIFG | UCTXIFG0 | UCRXIFG0);  //Clear interrupt flags
}


//*******************************
//         UI Functions
//*******************************
void _initICOM(void){
    initGPIO();
    initI2C();
}

/*
 * Summary: Enable all icom interrupts
 */
inline void _icom_IE(void){
    UCB0IE |= UCTXIE0 | UCSTPIE | UCRXIE0;        //Enable i2c transmit & i2c stop interrupt
}

/*
 * Summary: Disable all icom interrupts
 */
inline void _icom_ID(void){
    UCB0IE &= ~(UCTXIE0 | UCSTPIE | UCRXIE0);     //Disable i2c transmit & i2c stop interrupt
}

inline void _startUpdateRQ(void){
    P3OUT &= ~BIT0;
}

inline void _stopUpdateRQ(void){
    P3OUT |= BIT0;
}

void _preparePackage(uint8_t* bundle){
    icomPackage[0] = 0xBE;
    icomPackage[1] = 0xAF;
    if(bundle != NULL){
        //Prepare package with ui data
        icomPackage[2] = *bundle;
        bundle++;
        icomPackage[3] = *bundle;
        bundle++;
        icomPackage[4] = *bundle;
        bundle++;
        icomPackage[5] = *bundle;
        bundle++;
        icomPackage[6] = *bundle;
        bundle++;
        icomPackage[7] = *bundle;
        bundle++;
        icomPackage[8] = *bundle;
        bundle++;
        icomPackage[9] = *bundle;
        bundle++;
        icomPackage[10] = *bundle;
        bundle++;
        icomPackage[11] = *bundle;
    }else{
        //Prepare package with shutdown data
        icomPackage[11] = 0xFF;
    }


}


//*******************************
//         UI Event Handler
//*******************************
inline void __icom_I2C_Handler(bool* flag){
    switch(__even_in_range(UCB0IV, USCI_I2C_UCBIT9IFG))
    {
        case USCI_I2C_UCSTPIFG:     // STOP condition detected (master & slave mode)
           txCount = 0;
           *flag = true;
           break;

        case USCI_I2C_UCRXIFG0:     // RXIFG0
            rxBuff = UCB0RXBUF;
            break;

        case USCI_I2C_UCTXIFG0:     // TXIFG0
            UCB0TXBUF = icomPackage[txCount++];
            break;
    }
}
