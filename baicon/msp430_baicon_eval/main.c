#include <msp430fr5969.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "ui.h"
#include "icom.h"
#include "pwrsys.h"

typedef enum{
    START_WAKEUP,
    INTERFACE_MODE,
    SHUTDOWN,
} baiconState;


bool encoder_flag = false;
bool buttons_flag = false;
bool adc_flag = false;
bool i2c_tx_flag = false;
bool wake_up = false;
bool pipower_flag = false;

//====================================================================================================
//                                      HELPER DECLARATION
//====================================================================================================

void powerUpProcess(uint8_t* baiconState, uint8_t* bootState, long* tStamp);
void shutDownProcess(uint8_t* baiconState, uint8_t* shutdownState, long* tStamp);
void interfaceProcess(uint8_t* baiconState, uint8_t* interfaceState, long* tStamp, uint8_t *updateSource);

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
    PM5CTL0 &= ~LOCKLPM5;       //Unlock LPMx.5 configurations

    P1DIR |= BIT0;
    P4DIR |= BIT6;
    P1OUT &= ~BIT0;
    P4OUT &= ~BIT6;
    P2DIR |= BIT5 | BIT4;
    P2OUT &= ~(BIT5 | BIT4);


    uint8_t baiconState = START_WAKEUP;
    uint8_t bootState = POWER_TRIGGER;
    uint8_t interfaceState = SCAN;
    uint8_t shutdownState = TRANSPORT;
    uint8_t updateSource = 0;

    long tStamp = 0;

    _EINT();
    _initPwrSys();
    _initICOM();
    _initUI();
    _ui_ID();
    _icom_ID();
    _pwr_IE();

    while (1)
    {
        switch (baiconState){

            case START_WAKEUP:
            //Focuses on the start and wake up procedure of the baicon
                powerUpProcess(&baiconState, &bootState, &tStamp);
            break;


            case INTERFACE_MODE:
                interfaceProcess(&baiconState, &interfaceState, &tStamp, &updateSource);
            break;

            case SHUTDOWN:
            //Procedures the shutdown 
                shutDownProcess(&baiconState, &shutdownState, &tStamp);
                
            break;
        }
    }
}

//====================================================================================================
//                                      HELPER
//====================================================================================================

void powerUpProcess(uint8_t* baiconState, uint8_t* bootState, long* tStamp){
    switch (*bootState){
    //Handles the start and wake up procedure of the baicon

        case POWER_TRIGGER:
        //Checks if the powerline is constantly high to prevent single peak detection

            if (_getPwrLineMonitor()){
                *tStamp = _getSysCount();
                while ((_getSysCount() - *tStamp) < SYS_PWR_HOLD_TIME);    //Wait for 0.5s to ensure stable power supply
                
                if (_getPwrLineMonitor()){
                //Powerline stable start RPI
                    *bootState = PI_BOOT;
                    _buck_Enable();
                    *tStamp = _getSysCount();    //Starting time for RPI boot time control
                }

                /*Go back to sleep if only a peak was detected - function missing here*/
            }
        break;

        case PI_BOOT:
        //Handles the monitoring of the boot process of the RPI

            if (_getRpiPowerMonitor()){
                //PI booted successfully
                *bootState = INTERFACE_PREPARATION;
                _initICOM();
                _icom_IE();
            }

            if ((_getSysCount() - *tStamp) >= SYS_PIBOOT_TIMEOUT){
                //PI boot time reached timeout
                _buck_Disable(); 
                *bootState = POWER_TRIGGER;
            }

        break;

        case INTERFACE_PREPARATION:
            if (_getPwrLineMonitor()){
                //Powerline still supplies system
                _initUI();
                _ui_IE();
                *bootState = POWER_TRIGGER;
                *baiconState = INTERFACE_MODE;
                P1OUT |= BIT0;
            }else{
                //Battery supplies system
                P4OUT |= BIT6;
                *bootState = POWER_TRIGGER;
                *baiconState = SHUTDOWN;
                _preparePackage(NULL);
                _startUpdateRQ();
            }
        break;
    }
}

void shutDownProcess(uint8_t* baiconState, uint8_t* shutdownState, long* tStamp){
    switch (*shutdownState){
        case TRANSPORT:
            if (i2c_tx_flag){
                _stopUpdateRQ();
                *shutdownState = PI_SHUTDOWN;
                *tStamp = _getSysCount();
                i2c_tx_flag = false;
            }
        break;

        case PI_SHUTDOWN:
            if (!_getRpiPowerMonitor()){
                //PI shutted down successfully
                _buck_Disable();
                *shutdownState = GO_SLEEP;
                _icom_ID();
                _ui_ID();
                _pwr_ID();
            }

            if ((_getSysCount() - *tStamp) > SYS_PISHUTDOWN_TIMEOUT){
                //PI shutdown reached timout
                _preparePackage(NULL);
                _startUpdateRQ();
                *shutdownState = TRANSPORT;     //try again
            }
        break;

        case GO_SLEEP:
            P2OUT |= BIT5;
            _enableWakeUpDetection();
            if (wake_up){
                *shutdownState = TRANSPORT; //Reset shutdown FSM
                *baiconState = START_WAKEUP;
                wake_up = false;
                _disableWakeUpDetection();
                _initPwrSys();
                _pwr_IE();
            }
        break;
    }
}

void interfaceProcess(uint8_t* baiconState, uint8_t* interfaceState, long* tStamp, uint8_t *updateSource){

    if (!_getPwrLineMonitor()){
        //Power loss on power line detected
        _preparePackage(NULL);
        _startUpdateRQ();
        *baiconState = SHUTDOWN;
    }

    buttons_flag = _alteredButtonPort();

    switch(*interfaceState){
        case SCAN:
        {
            bool doUpdate = false;

            if (encoder_flag){
                doUpdate = true;
                *updateSource = encoder;
                encoder_flag = false;
            }

            if (buttons_flag){
                doUpdate = true;
                *updateSource = button;
                buttons_flag = false;
            }

            if (adc_flag){
                if(_compareKnobs()){
                    doUpdate = true;
                    *updateSource = knobs;
                }
                adc_flag = false;
            }

            if(doUpdate){
                uint8_t uiBundle[UI_BUNDLE_SIZE];
                _preparePackage(_getUIBundle(uiBundle, *updateSource));
                _prepareInterfaceMode();
                *tStamp = _getSysCount();
                _startUpdateRQ();
                *interfaceState = TRANSMIT;
            }

        break;
        }


        case TRANSMIT:

            if ((_getSysCount() - *tStamp) > ICOM_TX_TIMEOUT){
                //Transmission to PI reached timout
                _stopUpdateRQ();
                *interfaceState = SCAN;     //Scan and try again
            }
            
            if(i2c_tx_flag){
                _stopUpdateRQ();
                _updateTransmittedKnobs();
                i2c_tx_flag = false;
                *interfaceState = SCAN;     //try again
            }

        break;
    }

}
//====================================================================================================
//                                     ISRs
//====================================================================================================

#pragma vector=PORT3_VECTOR
__interrupt void Port_3(void)
{
    if (P3IFG)
    {
        __ui_Encoder_Handler(&encoder_flag);
    }
}

#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
{
    if (P2IFG & 0xFE)
    {
        __ui_Button_Handler(&buttons_flag);
    }
}

// ADC12 interrupt service routine
#pragma vector = ADC12_VECTOR
__interrupt void ADC12_ISR(void)
{
    __ui_Adc_Handler(&adc_flag);
}

// Timer0_A0 interrupt service routine
#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer0_A0_ISR(void)
{
    __ui_Timer_Handler();
}

// Timer A1 interrupt service routine
#pragma vector = TIMER1_A0_VECTOR
__interrupt void Timer1_A0_ISR(void)
{
    __sys_Timer_Handler();
}

//USCIB0_ISR interrupt service routine
#pragma vector=USCI_B0_VECTOR
__interrupt void USCIB0_ISR(void)
{
    __icom_I2C_Handler(&i2c_tx_flag);
}

#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{
    if (P1IFG & PLM_OFFSET)
    {
        __pwr_PowerLine_Handler(&wake_up);
    }
    else if (P1IFG & RPI_PM_OFFSET)
    {
        __pwr_RPIMonitor_Handler(&pipower_flag);
    }
}
