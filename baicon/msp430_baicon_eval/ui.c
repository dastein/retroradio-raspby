/*
 * ui.c
 *
 *  Created on: 11.11.2018
 *      Author: mgeik
 */
#include <math.h>

#include "ui.h"
#include "msp430fr5969.h"


uint8_t encPos = ENCODER_STEPS;
uint16_t actKnobs[3];
uint8_t previousBPort = 0;


uint16_t txKnobs[3];


//*******************************
//    Libary Support Functions
//*******************************

inline void initTimer(void){
    // Configure Timer0_A
    TA0CCR0 = CONV_PERIOD_COUNT;                //Conversion Period
    TA0CTL = TASSEL__SMCLK | MC__UP | ID_1;     // SMCLK, UP mode, Clock divide by 2 = 0.1s @CCR = 50000
}

void initADC(void){
    // Configure ADC12
    ADC12CTL0 = ADC12SHT0_2 | ADC12ON | ADC12MSC; // Sampling time, S&H=16, ADC12 on
    ADC12CTL1 = ADC12SHP | ADC12CONSEQ_1;         // Use sampling timer, set autorun mode
    ADC12CTL2 |= ADC12RES_2;                      // 12-bit conversion results
    ADC12CTL3 |= ADC12CSTARTADD_0;                // Start adress autoscan
    ADC12MCTL2 |= ADC12EOS;                       // Set stop adress of sequence
    ADC12MCTL0 |= ADC12INCH_3;                    // A3 ADC input select; Vref=AVCC
    ADC12MCTL1 |= ADC12INCH_5;                    // A1 ADC input select; Vref=AVCC
    ADC12MCTL2 |= ADC12INCH_10;                   // A5 ADC input select; Vref=AVCC
    ADC12CTL0 |= ADC12ENC;                        // Enable conversions
}

inline void initGPIO_ADC(void){
    P1SEL1 |= BIT3;                           // Configure P1.3 for ADC (A3)
    P1SEL0 |= BIT3;

    P1SEL1 |= BIT5;                           // Configure P1.5 for ADC (A5)
    P1SEL0 |= BIT5;

    P4SEL1 |= BIT2;                           // Configure P4.2 for ADC (A10)
    P4SEL0 |= BIT2;
}

void initGPIO_Encoder(void){
    P3DIR &= ~(BIT5 | BIT6);

    P3REN |= (BIT5 | BIT6); //Enable pull -up or -down resistor
    P3OUT |= (BIT5 | BIT6); //Select pullup resistor
    P3IES |= BIT5; //Select interrupt on falling edge
    P3IFG &= ~BIT5;    //Clear interrupt flag register
}

void initGPIO_Buttons(void){
    P2DIR &= ~(BUTT1 | BUTT2 | BUTT3 | BUTT4 | BUTT5 | BUTT6);
    P2REN |= (BUTT1 | BUTT2 | BUTT3 | BUTT4 | BUTT5 | BUTT6); //Enable pull -up or -down resistor
    P2OUT |= (BUTT1 | BUTT2 | BUTT3 | BUTT4 | BUTT5 | BUTT6); //Select pullup resistor
    P2IES &= ~(BUTT1 | BUTT2 | BUTT3 | BUTT4 | BUTT5 | BUTT6); //Select interrupt on rising edge
    P2IFG &= ~(BUTT1 | BUTT2 | BUTT3 | BUTT4 | BUTT5 | BUTT6);    //Clear interrupt flag register
}

//*******************************
//         UI Functions
//*******************************

void _initUI(){
    initGPIO_Encoder();
    initGPIO_Buttons();
    initGPIO_ADC();
    initADC();
    initTimer();
}

/*
 * Summary: Enable all user interface interrupts
 */
inline void _ui_IE(void){
    TA0CCTL0 = CCIE;            // TACCR0 interrupt enabled
    P3IE |= BIT5;               //Enable GPIO interrupt
    //P2IE |= (BUTT1 | BUTT2 | BUTT3 | BUTT4 | BUTT5 | BUTT6);  //Enable GPIO interrupt
    ADC12IER0 |= ADC12IE0 | ADC12IE1 | ADC12IE2;  //Enable ADC conversion complete interrupts

}

/*
 * Summary: Disable all user interface interrupts
 */
inline void _ui_ID(void){
    TA0CCTL0 &= ~CCIE;                                          //Disable TACCR0 interrupt
    P3IE &= ~BIT5;                                              //Disable encoder GPIO interrupt
    //P2IE &= ~(BUTT1 | BUTT2 | BUTT3 | BUTT4 | BUTT5 | BUTT6);   //Disable button GPIO interrupt
    ADC12IER0 &= (ADC12IE0 | ADC12IE1 | ADC12IE2);              //Disable ADC conversion complete interrupts
}

/*
 * Summary: Updates the last transmitted ADC values.
 */
inline void _updateTransmittedKnobs(void){
    txKnobs[0] = actKnobs[0];
    txKnobs[1] = actKnobs[1];
    txKnobs[2] = actKnobs[2];
}


/*
 * Summary: Determines the espsilon between between last transmitted and actual adc values.
 * Exceeding this boundaries will cause an update request.
 *
 * Returns: Update request state
 */
bool _compareKnobs(){
    bool bRet = false;
    int i = 0;
    for(i = 0; i<3; i++){
        if(abs(txKnobs[i]-actKnobs[i]) >= KNOB_EPSILON){
            //Knob position exceed tolerance lines from last transmitted value
            bRet = true;    //ask for an update request
        }
    }
    return bRet;
}

/*
* Summary: Fills all interface element states into a data bundle
* Parameter: bundle: Address of the bundle array, source: Source which triggered the update request
* Returns: Adress of the bundle array
*/
uint8_t* _getUIBundle(uint8_t* bundle, uint8_t source){
    uint16_t buttonPort = _getButtonPort();
    bundle[0] = (buttonPort >> 8) & 0xFF;
    bundle[1] = (buttonPort) & 0xFF;
    bundle[2] = encPos;
    bundle[3] = (actKnobs[0]>>8) & 0x0F;    
    bundle[4] = (actKnobs[0]) & 0xFF;
    bundle[5] = (actKnobs[1]>>8) & 0x0F;    
    bundle[6] = (actKnobs[1]) & 0xFF;    
    bundle[7] = (actKnobs[2]>>8) & 0x0F;    
    bundle[8] = (actKnobs[2]) & 0xFF;    
    bundle[9] = source;    

    return bundle;
}

inline bool _alteredButtonPort(void){
    bool bRet = false;
    uint8_t bPort = P2IN & BUTT_PORT_MASK;
    if((bPort - previousBPort) != 0){
    //A button state has changed
        previousBPort = bPort;
        bRet = true;
    }
    return bRet;
}

inline uint8_t _getButtonPort(void){
    return P2IN & BUTT_PORT_MASK;
}

//*******************************
//         UI Event Handler
//*******************************

inline void __ui_Encoder_Handler(bool *flag){

    if (P3IN & ENC_B){
        encPos++;
    }else{
        encPos--;
    }
    *flag = true;
    P3IFG &= ~ENC_A;    //Clear interrupt flag register

    //do here some debouncing
}

inline void __ui_Button_Handler(bool *flag){
    *flag = true;
    P2IFG &= ~(BUTT1 | BUTT2 | BUTT3 | BUTT4 | BUTT5 | BUTT6);    //Clear interrupt flag register
    //do here some debouncing
}

inline void __ui_Adc_Handler(bool* flag){
    switch(__even_in_range(ADC12IV, ADC12IV_ADC12RDYIFG))
      {
        case ADC12IV_ADC12IFG0:                 // Vector 12:  ADC12MEM0 Interrupt
            actKnobs[0] = ADC12MEM0;
            break;                                  // Clear CPUOFF bit from 0(SR)

        case ADC12IV_ADC12IFG1:                     // Vector 14:  ADC12MEM1
            actKnobs[1] = ADC12MEM1;
            break;

        case ADC12IV_ADC12IFG2:
            actKnobs[2] = ADC12MEM2;
            *flag = true;
            break;                                  // Vector 16:  ADC12MEM2

        default: break;
      }
}

inline void __ui_Timer_Handler(){
    ADC12CTL0 |= ADC12SC;     //start sampling conversation
}

