/*
 * ui.h
 *
 *  Created on: 11.11.2018
 *      Author: mgeik
 */

#ifndef UI_H_
#define UI_H_

#include <stdint.h>
#include <stdbool.h>

#define UI_BUNDLE_SIZE 10

#define CONV_PERIOD_COUNT   25000
#define KNOB_EPSILON      200

#define ENCODER_STEPS   20
#define ENC_A   BIT5
#define ENC_B   BIT6

#define BUTT_PORT_MASK 0xFC
#define BUTT1   BIT5
#define BUTT2   BIT6
#define BUTT3   BIT2
#define BUTT4   BIT7
#define BUTT5   BIT3
#define BUTT6   BIT4

typedef enum{
    none = 0x00,
    button = 0x01,
    encoder = 0x02,
    knobs = 0x03,
}Event_Source;

//*******************************
//    Libary Support Functions
//*******************************
void initTimer(void);
void initADC(void);
void initGPIO_ADC(void);
void initGPIO_Encoder(void);
void initGPIO_Buttons(void);

//*******************************
//         UI Functions
//*******************************
void _initUI(void);

bool _compareKnobs(void);
extern void _updateTransmittedKnobs(void);
extern void _ui_IE(void);
extern void _ui_ID(void);

uint8_t* _getUIBundle(uint8_t* bundle, uint8_t source);
extern bool _alteredButtonPort(void);
extern uint8_t _getButtonPort(void);

//*******************************
//         UI Event Handler
//*******************************

extern void __ui_Encoder_Handler(bool *flag);
extern void __ui_Button_Handler(bool *flag);
extern void __ui_Adc_Handler(bool* flag);
extern void __ui_Timer_Handler(void);

#endif /* UI_H_ */
