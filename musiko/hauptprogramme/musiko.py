#!/usr/bin/python3

import json
import os
import signal
import sys
import subprocess

import dbus
import dbus.service
import dbus.mainloop.glib
from dbus.exceptions import DBusException

from pydbus import SystemBus

from gi.repository import GLib
from pathlib import Path

STANDARD_CONFIG = Path('/home/pi/retroradio-raspby/config-dateien/retroradio_config.json')
NOTFALL_CONFIG = Path('/home/pi/retroradio-raspby/config-dateien/JSON_Default_Config.json')

DB_PARAM = ["busctl", "--user", "call", " hm.retro.Visio", "/hm/retro/Visio", 
        "hm.retro.Visio", "new_index", "i", "10"]


# Musiko's own little class to update the display when a key event occurs
class Visio:
    def __init__(self):
        self.bus = SystemBus()

        self.obj = self.bus.get('hm.retro.Visio')

    def new_index(self, x):
        self.obj.new_index(x)


class Radio(dbus.service.Object):
    DBUS_NAME = 'hm.retro.Musiko'
    DBUS_OBJECT_PATH = '/hm/retro/Musiko'
    DBUS_INTERFACE = 'hm.retro.Musiko'

    def __init__(self, _disp):
        self.bus = dbus.SystemBus()
        bus_name = dbus.service.BusName(self.DBUS_NAME, bus=self.bus)
        self.mpv = None
        self.number_of_stations = 0
        self.cfg = self.load_config()

        # Variable used to store the current index in json-config
        self.last_index = 0

        self.disp = _disp

        # Variable zum ordentlichen Neustarten
        self.player_lebt = False
        super().__init__(bus_name, self.DBUS_OBJECT_PATH)


    def load_config(self):
        if STANDARD_CONFIG.exists():
            with STANDARD_CONFIG.open() as f:
                konfig = json.load(f)
                print("User-Config geladen.", file=sys.stderr)
        elif NOTFALL_CONFIG.exists():
            with NOTFALL_CONFIG.open() as f:
                konfig = json.load(f)
                print("Notfall-Config geladen", file=sys.stderr)
        else:
            print("Katastrophaler Fehler: Keine Config-Dateien. Beende...", 
                    file=sys.stderr)
            sys.exit(1)

        self.number_of_stations = len(konfig["senderliste"])
        return konfig

    # gets an int with the desired keynr. negative if delta-mode requested
    @dbus.service.method(DBUS_INTERFACE, in_signature='ii')
    def start_radio(self, taste, delta):
        print("start_radio called", file=sys.stderr)
        new_index = 0

        # Don't do anything if the player already runs
        if self.player_lebt:
            raise DBusException('player already runs')
            return

        # Parse the url which shall be played. If the key code is negative
        # we're in delta-mode and calculate the new index in the json-config
        # otherwise we interpret 'taste' and get the desired index using the
        # preset-list. abs() as security if delta is awkward
        # a given -2 means we shall play the station we played before
        if delta != 0 and taste == -1:
            new_index = (self.last_index+delta) % self.number_of_stations
        elif taste == -2:
            new_index = self.last_index
        else:
            new_index = self.cfg["presetliste"][abs(taste)]
        self.last_index = new_index


        # If the index is -1 it means that the Key is not mapped to a station yet
        # Falls Taste nicht belegt, starte Sender mit Index = Tastennr.
        url = self.cfg["senderliste"][new_index]["url"]

        # if the url for the index>=0 in the json-config is -1 it means that
        # there is no entry
        if url == -1:
            raise DBusException('Eintrag nicht vorhanden!')
            return

        args = ['mpv', '--no-video', '--no-ytdl', url]
        schalter = GLib.SpawnFlags.SEARCH_PATH | GLib.SpawnFlags.DO_NOT_REAP_CHILD \
                    | GLib.SpawnFlags.STDOUT_TO_DEV_NULL | \
                    GLib.SpawnFlags.STDERR_TO_DEV_NULL

        # https://lazka.github.io/pgi-docs/#GLib-2.0/functions.html#GLib.spawn_async
        # Spawnen des mpv-Players in den Hintergrund
        self.mpv, _, _, _ = GLib.spawn_async(args, flags=schalter)
        self.player_lebt = True

        # inform display (stupid solution, but works...)
        self.disp.new_index(new_index)

    @dbus.service.method(DBUS_INTERFACE)
    def stop_radio(self):
        print("stop_radio called", file=sys.stderr)

        # thanks to steuermann.py this should only occur if mpv dies abnormally
        if not self.player_lebt:
            raise DBusException('player does not run')
            return

        # Kill the child and wait until it is dead.
        os.kill(self.mpv, signal.SIGTERM)
        _, status = os.waitpid(self.mpv, 0)
        self.player_lebt = False

    @dbus.service.method(DBUS_INTERFACE)
    def config_change_musiko(self):
        self.cfg = self.load_config()
        # NUMBER_OF_STATIONS = 

        # If a config_change occurs while running, start playing Preset No.0
        if self.player_lebt:
            self.stop_radio()
        self.start_radio(0, 0)
        print("Config Change ausgeführt!", file=sys.stderr)


def main():
    # Die DBUS-Eventloop ist die Mainloop der GLib. Scheinbar ist diese
    # gegenwärtig die einzige unterstützte Mainloop.
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    loop = GLib.MainLoop()

    disp = Visio()

    # Instanziieren der Dbus-Service-Klasse
    service = Radio(disp)

    loop.run()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
