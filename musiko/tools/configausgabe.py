import json

def main():
    with open("../Sonstiges/Beispiel_10_Sender.json") as datei:
        konfig = json.load(datei)

    print(konfig)
    # print(konfig['Sender'][0]['Sendername'])

    for x in konfig["Sender"]:
        print(x["Sendername"], x["URL"])


if __name__ == '__main__':
    main()
