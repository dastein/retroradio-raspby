import smbus2 as smbus
import RPi.GPIO as GPIO
import time

"""======================================================
                    REGITSTER ADRESSES
======================================================"""

CLOCK_CONTROL = 0x00  # CLOCK CONTROL REG / Single
DEVICE_ID = 0x01 # Device ID Register / Single
ERROR_STATUS = 0x02 # Error Status Register / Single
SYSTEM_CONTROL_1 = 0x03 # System Control Register 1 / Single
SERIAL_DATA_INTERFACE = 0x04  # Serial Data Interface Register / Single
SYSTEM_CONTROL_2 = 0x05  # System Control Register 2 / Single
SOFT_MUTE = 0x06 # Soft Mute Register / Single
VOLUME_MASTER = 0x07  # Volume Control Register Master / Single
VOL_CH1 = 0x08 # Volume Channel 1 / Single
VOL_CH2 = 0x09 # Volume Channel 2 / Single
VOL_CONFIG = 0x0E # Volume Configuration Register / Single
MODULATION_LIMIT = 0x10 # Modulation Limit Register / Single
INTERCHANNEL_DELAY_1 = 0x11 # Interchannel Delay Register 1 / Single
INTERCHANNEL_DELAY_2 = 0x12 # Interchannel Delay Register 2 / Single
INTERCHANNEL_DELAY_N1 = 0x13 # Interchannel Delay Register n1 / Single
INTERCHANNEL_DELAY_N2 = 0x14 # Interchannel Delay Register n2 / Single
PWM_SHTDWN_GROUP = 0x19 # PWM Shutdown Group Register / Single
STARTSTOP_PERIOD = 0x1A  # Start Stop Period Control Register / Single
OSC_TRIM = 0x1B # Oscillator Trim Register / Single
BKND_ERR = 0x1C # Back-End Error Signal / Single
INPUT_MUX = 0x20 # Input Mutliplexer / Multi 4 Byte
PWM_OUTPUT_MUX = 0x25 # PWM Output MUX Register / Multi 4 Byte
AGL_CONTROL = 0x46 # AGL Control Register / Multi 4 Byte
PWM_SWITCHING = 0x4F # PWM Switching Rate Control / 4 Byte
BANK_SWITCH_EQ_CONTROL = 0x50  # Bank Switch and EQ Control / Multi 4 Byte

"""======================================================
                        COMMANDS
======================================================"""

# CCR
SR_48KHZ = 0x60
MCLK_256 = 0x0C
MCLK_64 = 0x00

# SDIR
I2S_24 = 0x05
I2S_16 = 0x03
RJUST = 0x00
LJUST = 0x06

# SCR2
EXIT_SHTDWN = 0x00
ENTER_SHTDWN = 0x40

# BSEQ
EQ_ON = 0x00
EQ_OFF = 0x80
BSEQ_RSVD = 0xF700000

# VOLRM
VOL_MAX = 0x00
VOL_MIN = 0x3FF
VOL_BW = 0x3FF

#SSPR
SSPR_RSVD = 0x60
SSTIM_EN = 0x00
SSTIM_DIS = 0x80
DC_OFF = 0x00
T16_5 = 0x08
T53_9 = 0x0B
T125_7 = 0x0F
T538_6 = 0x14
T9897_3 = 0x1E

#VOL_CONFIG
VOLCONF_RSVD = 0x90

class AudioControl:
    """This class controls and handles the communication between RIIO SS and the AMO"""

    def __init__(self, logger = None, i2c_bus, slave_address):
        self.logger = logger
        self.i2c_bus = i2c_bus
        self.slave_address = slave_address
        self.amp_pdn_pin = 13
        self.amp_rst_pin = 6
        self.amp_adr_fault_Pin = 5
        self.soft_startstop_time = 0.01      # has to be set here in sec
        #initialize registers
        self.conf_CLOCK_CONTROL = 0x00
        self.conf_DEVICE_ID = 0x00
        self.conf_ERROR_STATUS = 0x00
        self.conf_SYSTEM_CONTROL_1 = 0x00
        self.conf_SERIAL_DATA_INTERFACE = 0x00
        self.conf_SYSTEM_CONTROL_2 = 0x00
        self.conf_SOFT_MUTE = 0x00
        self.conf_VOLUME_MASTER = [0x03, 0xFF]
        self.conf_VOL_CH1 = 0x00
        self.conf_VOL_CH2 = 0x00
        self.conf_VOL_CONFIG = VOLCONF_RSVD
        self.conf_MODULATION_LIMIT = 0x00
        self.conf_INTERCHANNEL_DELAY_1 = 0x00
        self.conf_INTERCHANNEL_DELAY_2 = 0x00
        self.conf_INTERCHANNEL_DELAY_N1 = 0x00
        self.conf_INTERCHANNEL_DELAY_N2 = 0x00
        self.conf_PWM_SHTDWN_GROUP = 0x00
        self.conf_STARTSTOP_PERIOD = SSPR_RSVD
        self.conf_OSC_TRIM = 0x00
        self.conf_BKND_ERR = 0x00
        self.conf_INPUT_MUX = []
        self.conf_PWM_OUTPUT_MUX = []
        self.conf_AGL_CONTROL = []
        self.conf_PWM_SWITCHING = []
        self.conf_BANK_SWITCH_EQ_CONTROL = []


    def start(self, initial_volume, initial_eq = None):
        """Handles Start-Up Sequence of amplifier"""
        
        #================================CONFIGUE REGISTER CHANGES HERE===================================
        self.conf_CLOCK_CONTROL = SR_48KHZ + MCLK_256
        self.conf_SERIAL_DATA_INTERFACE = I2S_16
        self.conf_STARTSTOP_PERIOD = SSPR_RSVD + SSTIM_DIS + DC_OFF
        self.conf_VOL_CONFIG = VOLCONF_RSVD
        self.conf_BANK_SWITCH_EQ_CONTROL = (BSEQ_RSVD + EQ_OFF).to_bytes(4, byteorder='big')
        self.conf_SYSTEM_CONTROL_2 = EXIT_SHTDWN
        #=================================================================================================

        # Drive _RST = 0, _PDN = 0 & wait at least 13.5 ms
        GPIO.output(self.amp_rst_pin, 1)
        GPIO.output(self.amp_pdn_pin, 1) 
        time.sleep(0.015)
        self.complete_register_readout(readout_message="Initial Register State", logging=True, log_format="DUAL")
        self.logger.log(">> Audio Amplifier Start-Up Sequence started")
        self.logger.log("\t-- Send Trim command")
        #Trim oscillator and wait at least 50 ms
        self.set_OSC_TRIM(0x00)
        time.sleep(0.055)
        self.logger.log("\t-- Configure Digital Audio Processor")
        # Configure the Digital Audio Processor of the Amplifier
        self.set_CLOCK_CONTROL(self.conf_CLOCK_CONTROL)
        self.set_SERIAL_DATA_INTERFACE(self.conf_SERIAL_DATA_INTERFACE) # Set I2S input (DEFAULT) / Set I2S 16bit word length
        self.set_STARTSTOP_PERIOD(self.conf_STARTSTOP_PERIOD) # Set soft start/stop period 
        self.set_VOL_CONFIG(self.conf_VOL_CONFIG)
        self.set_BANK_SWITCH_EQ_CONTROL(self.conf_BANK_SWITCH_EQ_CONTROL)

        self.logger.log("\t-- Exit all channel shutdown")
        #Exit all channel shutdown and wait at least 1ms + 1.3 t_start set in SSPR
        self.set_SYSTEM_CONTROL_2(self.conf_SYSTEM_CONTROL_2) # Exit all-channel shutdown to enter normal operation
        time.sleep((self.soft_startstop_time*1.3)*1.1)  # 10 % secure range
        self.logger.log("\t-- done")
        self.logger.log(">> Normal Operation Mode entered")
        #set start volume
        self.change_volume_settings(initial_volume)
        self.complete_register_readout("Configured Register Values", True, "DUAL")

        

    def stop(self, hard_shutdown=False):
        """Handles power down sequence of Amplifier"""
        self.logger.log(">> Power-Down Sequence started")
        if hard_shutdown is False:
            self.logger.log("\t--execute soft shutdown")
            # Write 0x40 to register 0x05 and wait at least 1ms + 1.3*t_stop
            self.set_SYSTEM_CONTROL_2(ENTER_SHTDWN)
            time.sleep(0.01 + self.soft_startstop_time)
        else:
            # Assert _PDN = 0 and wait at least 2 ms
            self.logger.log("\t--execute hard shutdown")
            GPIO.output(self.amp_pdn_pin, 0)
            time.sleep(0.01)
        # Assert _RST = 0
        GPIO.output(self.amp_rst_pin, 0)
        self.logger.log("\t--done")

    def init_gpio(self):
        """Inits all GPIOs with audio responsibility"""
        time.sleep(1)
        GPIO.setup(self.amp_pdn_pin, GPIO.OUT)
        GPIO.setup(self.amp_rst_pin, GPIO.OUT)
        from riio_session import AUDIO_SLA_ADDR_PU, AUDIO_SLA_ADDR_PD
        if self.slave_address == AUDIO_SLA_ADDR_PU:
            """Set RPI pullup to set amplifier slave address to 0x56"""
            GPIO.setup(self.amp_adr_fault_Pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # Slave address hardware select
        elif self.slave_address == AUDIO_SLA_ADDR_PD:
            """Set RPI pulldown to set amplifier slave address to 0x54"""
            GPIO.setup(self.amp_adr_fault_Pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)  # Slave address hardware select
        GPIO.output(self.amp_pdn_pin, 1) 
        GPIO.output(self.amp_rst_pin, 1)


    def change_volume_settings(self, volume):
        """Changes the volume to the selected volume"""
        self.volume_to_bytes(volume)
        self.set_VOLUME_MASTER(self.conf_VOLUME_MASTER)


    def change_volume_mute(self):
        """Mutes the volume"""
        self.change_volume_settings(0)


    def change_volume_unmute(self, destination_volume):
        """Unmutes the volume and set the selected destination volume"""
        self.change_volume_settings(destination_volume)


    def volume_to_bytes(self, volume):
        """Converts the volume in amplifier range/format"""
        vol_factor = 1-volume
        byte_volume = int(VOL_BW*vol_factor)
        self.conf_VOLUME_MASTER = byte_volume.to_bytes(2, byteorder='big')


    def change_eq_settings(self):
        pass
    
    """======================================================
                TAS5753MD REGISTER METHODS
    ======================================================"""

    def complete_register_readout(self, readout_message="Complete Register Readout", logging=True, log_format="HEX"):
        """Reads complete register settings """
        if logging is True:
            self.logger.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
            self.logger.log(">> " + readout_message)
            self.logger.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

        self.get_CLOCK_CONTROL(logging, log_format)
        self.get_DEVICE_ID(logging, log_format)
        self.get_ERROR_STATUS(logging, log_format)
        self.get_SYSTEM_CONTROL_1(logging, log_format)
        self.get_SERIAL_DATA_INTERFACE(logging, log_format)
        self.get_SYSTEM_CONTROL_2(logging, log_format)
        self.get_SOFT_MUTE(logging, log_format)
        self.get_VOLUME_MASTER(logging, log_format)
        self.get_VOL_CH1(logging, log_format)
        self.get_VOL_CH2(logging, log_format)
        self.get_VOL_CONFIG(logging, log_format)
        self.get_MODULATION_LIMIT(logging, log_format)
        self.get_INTERCHANNEL_DELAY_1(logging, log_format)
        self.get_INTERCHANNEL_DELAY_2(logging, log_format)
        self.get_INTERCHANNEL_DELAY_N1(logging, log_format)
        self.get_INTERCHANNEL_DELAY_N2(logging, log_format)
        self.get_PWM_SHTDWN_GROUP(logging, log_format)
        self.get_STARTSTOP_PERIOD(logging, log_format)
        self.get_OSC_TRIM(logging, log_format)
        self.get_BKND_ERR(logging, log_format)
        self.get_INPUT_MUX(logging, log_format)
        self.get_PWM_OUTPUT_MUX(logging, log_format)
        self.get_AGL_CONTROL(logging, log_format)
        self.get_PWM_SWITCHING(logging, log_format)
        self.get_BANK_SWITCH_EQ_CONTROL(logging, log_format)
        self.logger.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        self.logger.log(">>")

    
    def get_CLOCK_CONTROL(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, CLOCK_CONTROL)
        if logging is True:
            self.logging_wrapper("(0x00) CLOCK CONTROL Register", reg, log_format)
        return reg
    
    def get_DEVICE_ID(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, DEVICE_ID)
        if logging is True:
            self.logging_wrapper("(0x01) DEVICE_ID Register", reg, log_format)
        return reg
    
    def get_ERROR_STATUS(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, ERROR_STATUS)
        if logging is True:
            self.logging_wrapper("(0x02) ERROR_STATUS Register", reg, log_format)
        return reg
    
    def get_SYSTEM_CONTROL_1(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, SYSTEM_CONTROL_1)
        if logging is True:
            self.logging_wrapper("(0x03) SYSTEM_CONTROL_1 Register", reg, log_format)
        return reg
    
    def get_SERIAL_DATA_INTERFACE(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, SERIAL_DATA_INTERFACE)
        if logging is True:
            self.logging_wrapper("(0x04) SERIAL_DATA_INTERFACE Register", reg, log_format)
        return reg
    
    def get_SYSTEM_CONTROL_2(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, SYSTEM_CONTROL_2)
        if logging is True:
            self.logging_wrapper("(0x05) SYSTEM_CONTROL_2 Register", reg, log_format)
        return reg
    
    def get_SOFT_MUTE(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, SOFT_MUTE)
        if logging is True:
            self.logging_wrapper("(0x06) SOFT_MUTE Register", reg, log_format)
        return reg
        
    def get_VOLUME_MASTER(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_i2c_block_data(self.slave_address, VOLUME_MASTER, 2)
        if logging is True:
            self.logging_wrapper("(0x07) VOLUME_MASTER Register", reg, log_format)
        return reg
    
    def get_VOL_CH1(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_i2c_block_data(self.slave_address, VOL_CH1, 2)
        if logging is True:
            self.logging_wrapper("(0x08) VOL_CH1 Register", reg, log_format)
        return reg
    
    def get_VOL_CH2(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_i2c_block_data(self.slave_address, VOL_CH2, 2)
        if logging is True:
            self.logging_wrapper("(0x09) VOL_CH2 Register", reg, log_format)
        return reg
    
    def get_VOL_CONFIG(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, VOL_CONFIG)
        if logging is True:
            self.logging_wrapper("(0x0E) VOL_CONFIG Register", reg, log_format)
        return reg
    
    def get_MODULATION_LIMIT(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, MODULATION_LIMIT)
        if logging is True:
            self.logging_wrapper("(0x10) MODULATION_LIMIT Register", reg, log_format)
        return reg
    
    def get_INTERCHANNEL_DELAY_1(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, INTERCHANNEL_DELAY_1)
        if logging is True:
            self.logging_wrapper("(0x11) INTERCHANNEL_DELAY_1 Register", reg, log_format)
        return reg
    
    def get_INTERCHANNEL_DELAY_2(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, INTERCHANNEL_DELAY_2)
        if logging is True:
            self.logging_wrapper("(0x12) INTERCHANNEL_DELAY_2 Register", reg, log_format)
        return reg
    
    def get_INTERCHANNEL_DELAY_N1(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, INTERCHANNEL_DELAY_N1)
        if logging is True:
            self.logging_wrapper("(0x13) INTERCHANNEL_DELAY_N1 Register", reg, log_format)
        return reg
    
    def get_INTERCHANNEL_DELAY_N2(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, INTERCHANNEL_DELAY_N2)
        if logging is True:
            self.logging_wrapper("(0x14) INTERCHANNEL_DELAY_N2 Register", reg, log_format)
        return reg
    
    def get_PWM_SHTDWN_GROUP(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, PWM_SHTDWN_GROUP)
        if logging is True:
            self.logging_wrapper("(0x19) PWM_SHTDWN_GROUP Register", reg, log_format)
        return reg
    
    def get_STARTSTOP_PERIOD(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, STARTSTOP_PERIOD)
        if logging is True:
            self.logging_wrapper("(0x1A) STARTSTOP_PERIOD Register", reg, log_format)
        return reg
    
    def get_OSC_TRIM(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, OSC_TRIM)
        if logging is True:
            self.logging_wrapper("(0x1B) OSC_TRIM Register", reg, log_format)
        return reg
    
    def get_BKND_ERR(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_byte_data(self.slave_address, BKND_ERR)
        if logging is True:
            self.logging_wrapper("(0x1C) BKND_ERR Register", reg, log_format)
        return reg
    
    def get_INPUT_MUX(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_i2c_block_data(self.slave_address, INPUT_MUX, 4)
        if logging is True:
            self.logging_wrapper("(0x20) INPUT_MUX Register", reg, log_format)
        return reg
    
    def get_PWM_OUTPUT_MUX(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_i2c_block_data(self.slave_address, PWM_OUTPUT_MUX, 4)
        if logging is True:
            self.logging_wrapper("(0x25) PWM_OUTPUT_MUX Register", reg, log_format)
        return reg
    
    def get_AGL_CONTROL(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_i2c_block_data(self.slave_address, AGL_CONTROL, 4)
        if logging is True:
            self.logging_wrapper("(0x46) AGL_CONTROL Register", reg, log_format)
        return reg
    
    def get_PWM_SWITCHING(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_i2c_block_data(self.slave_address, PWM_SWITCHING, 4)
        if logging is True:
            self.logging_wrapper("(0x4F) PWM_SWITCHING Register", reg, log_format)
        return reg
    
    def get_BANK_SWITCH_EQ_CONTROL(self, logging = True, log_format = "HEX"):
        reg = self.i2c_bus.read_i2c_block_data(self.slave_address, BANK_SWITCH_EQ_CONTROL, 4)
        if logging is True:
            self.logging_wrapper("(0x50) BANK_SWITCH_EQ_CONTROL Register", reg, log_format)
        return reg
    
    
    def set_CLOCK_CONTROL(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, CLOCK_CONTROL, setts)
    
    def set_DEVICE_ID(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, DEVICE_ID, setts)
    
    def set_ERROR_STATUS(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, ERROR_STATUS, setts)
    
    def set_SYSTEM_CONTROL_1(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, SYSTEM_CONTROL_1, setts)
    
    def set_SERIAL_DATA_INTERFACE(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, SERIAL_DATA_INTERFACE, setts)
    
    def set_SYSTEM_CONTROL_2(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, SYSTEM_CONTROL_2, setts)
    
    def set_SOFT_MUTE(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, SOFT_MUTE, setts)
    
    def set_VOLUME_MASTER(self, setts):
        self.i2c_bus.write_i2c_block_data(self.slave_address, VOLUME_MASTER, bytes(setts))
    
    def set_VOL_CH1(self, setts):
        self.i2c_bus.write_i2c_block_data(self.slave_address, VOL_CH1, bytes(setts))
    
    def set_VOL_CH2(self, setts):
        self.i2c_bus.write_i2c_block_data(self.slave_address, VOL_CH2, bytes(setts))
    
    def set_VOL_CONFIG(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, VOL_CONFIG, setts)
    
    def set_MODULATION_LIMIT(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, MODULATION_LIMIT, setts)
    
    def set_INTERCHANNEL_DELAY_1(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, INTERCHANNEL_DELAY_1, setts)
    
    def set_INTERCHANNEL_DELAY_2(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, INTERCHANNEL_DELAY_2, setts)
    
    def set_INTERCHANNEL_DELAY_N1(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, INTERCHANNEL_DELAY_N1, setts)
    
    def set_INTERCHANNEL_DELAY_N2(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, INTERCHANNEL_DELAY_N2, setts)
    
    def set_PWM_SHTDWN_GROUP(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, PWM_SHTDWN_GROUP, setts)
    
    def set_STARTSTOP_PERIOD(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, STARTSTOP_PERIOD, setts)
    
    def set_OSC_TRIM(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, OSC_TRIM, setts)
    
    def set_BKND_ERR(self, setts):
        self.i2c_bus.write_byte_data(self.slave_address, BKND_ERR, setts)
    
    def set_INPUT_MUX(self, setts):
        self.i2c_bus.write_i2c_block_data(self.slave_address, INPUT_MUX, bytes(setts))
    
    def set_PWM_OUTPUT_MUX(self, setts):
        self.i2c_bus.write_i2c_block_data(self.slave_address, PWM_OUTPUT_MUX, bytes(setts))
    
    def set_AGL_CONTROL(self, setts):
        self.i2c_bus.write_i2c_block_data(self.slave_address, AGL_CONTROL, bytes(setts))
    
    def set_PWM_SWITCHING(self, setts):
        self.i2c_bus.write_i2c_block_data(self.slave_address, PWM_SWITCHING, bytes(setts))
    
    def set_BANK_SWITCH_EQ_CONTROL(self, setts):
        self.i2c_bus.write_i2c_block_data(self.slave_address, BANK_SWITCH_EQ_CONTROL, bytes(setts))
    
    
    def logging_wrapper(self, reg_des, reg_cont, format="HEX"):
        """Loggs register content in the desired format"""
        
        if type(reg_cont) is int:
            reg_int = reg_cont
        if type(reg_cont) is list:
            reg_int = int.from_bytes(bytes(reg_cont), byteorder="big")
            
        if format is "HEX":
            hex_msg = "{0}:\n\t{1:10}".format(reg_des, hex(reg_int))
            self.logger.log(hex_msg)
        elif format is "BIN":
            bin_msg = "{0}:\n\t{1}".format(reg_des, bin(reg_int))
            self.logger.log(bin_msg)
        elif format is "DUAL":
            dual_msg = "{0}:\n\t{1:10} | {2}".format(reg_des, hex(reg_int), bin(reg_int))
            self.logger.log(dual_msg)
            
