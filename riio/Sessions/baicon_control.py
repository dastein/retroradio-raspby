import RPi.GPIO as GPIO

"""======================================================
        POTENTIOMETER RANGE NORMALIZATION CONSTANTS
======================================================"""
KNOB1_VOL_MIN = 257
KNOB1_VOL_MAX = 3967
KNOB1_vOL_RANGE = KNOB1_VOL_MAX - KNOB1_VOL_MIN

KNOB2_EQ_HIGH_MIN = 366
KNOB2_EQ_HIGH_MAX = 3839
KNOB2_EQ_HIGH_RANGE = KNOB2_EQ_HIGH_MAX - KNOB2_EQ_HIGH_MIN

KNOB3_EQ_LOW_MIN = 390
KNOB3_EQ_LOW_MAX = 3840
KNOB3_EQ_LOW_RANGE = KNOB3_EQ_LOW_MAX - KNOB3_EQ_LOW_MIN

"""======================================================
        POTENTIOMETER FLUCTUATION CONSTANT
======================================================"""
POTI_FLUCT = 100

class BaiconControl:
    """This class controls and handles the communication between RIIO SS and the Baicon"""

    def __init__(self, debugLogger = None, i2cBus, slaveAdress):
        self.debugLogger = debugLogger
        self.update_rq_pin = 23
        self.pwr_mon_pin = 24
        self.i2cBus = i2cBus
        self.slaveAdress = slaveAdress
            
        self.first_message_flag = True
        self.last_decoded_message = {'Start_bytes':0x0000,
                                    'Buttonstate_bytes':0x0000,
                                    'Endoderpos_byte':0x00,
                                    'Knob1pos_bytes':0x0000,
                                    'Knob2pos_bytes':0x0000,
                                    'Knob3pos_bytes':0x0000,
                                    'Source_byte':0x00}
        self.decoded_message = self.last_decoded_message

        self.change_src = None
        self.volume = 0                 #in %
        self.mute_unmute_btn_aktive = 0 #1: mute 0:unmute
        self.eq = {"HIGH":0,"LOW":0}    #in %
        self.ch_delta_enc = 0
        self.ch_preset_btn = 1
        
    """======================================================
                    PRIVATE METHODS
    ======================================================"""
    def __update_vol_level(self,mute=False):
        """This method updates the volume level based on last decoded i2c message"""
        if mute == False:
            vol_adc_value = self.decoded_message["Knob1pos_bytes"]
            
            calc_vol_level = int((vol_adc_value / KNOB1_vOL_RANGE)*100)
            
            self.volume = calc_vol_level if calc_vol_level <= 100 else 100
        else:
            self.volume = 0
    
    def __update_eq_level(self):
        """This method updates the eq levels based on last decoded i2c message"""
        eq_adc_val_low = self.decoded_message["Knob3pos_bytes"]
        eq_adc_val_high = self.decoded_message["Knob2pos_bytes"]
        
        calc_eq_level_low  = int((eq_adc_val_low  /KNOB3_EQ_LOW_RANGE )*100)
        calc_eq_level_high = int((eq_adc_val_high /KNOB2_EQ_HIGH_RANGE)*100)
        
        self.eq["LOW"]  = calc_eq_level_low  if calc_eq_level_low  <= 100 else 100
        self.eq["HIGH"] = calc_eq_level_high if calc_eq_level_high <= 100 else 100
    
    """======================================================
                    PUBLIC METHODS
    ======================================================"""
    def get_source(self):
        """ This method returns the source of radio control change """
        return self.change_src

    def get_volume(self):
        """ This method returns the volume level as integer percentage value from 0 t0 100 """
        return self.volume

    def get_mute_state(self):
        """ This method returns the current radio volume mute state (1:muted , 0:unmuted) """
        return self.mute_unmute_btn_aktive

    def get_eq(self):
        """ This method returns the eq levels (high,low) as dict of integer percentage values from 0 t0 100 """
        return self.eq

    def get_ch_delta_enc(self):
        """ This method returns the encoder delta since last encoder value change as integer value (-1 , +1) """
        return self.ch_delta_enc

    def get_ch_preset_btn(self):
        """ This method returns the currently aktive channel preset btn number (0 to 4) """
        return self.ch_preset_btn
    
    def get_message_flag(self):
        """ This method returns the indicator for determining if initial message has been sent or not 
        (True:frist message , false: not first message) """
        return self.first_message_flag

    def init_gpio(self):
        """ This method configures all GPIO Pins needed for baicon-rpi kommunikation """
        if self.debugLogger != None:
            self.debugLogger.log("> Configuring baicon-rpi com Pins")
        # Set update request PIN as input, pulled up to avoid false detection.
        GPIO.setup(UPDATE_RQ_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) 

        # Set power monitor PIN as output
        GPIO.setup(self.pwr_mon_pin,GPIO.OUT)
        # Set power monitor PIN output to high
        GPIO.output(self.pwr_mon_pin,1)

    def get_ui(self):
        """
        This function will initiate the i2c multibyte read communikation, read raw data &
        parses + decode the raw byte array
        """

        self.debugLogger.log("> Trying to read data block from slave transmitter: ",hex(self.slaveAdress))
        read_byte_array = self.i2cBus.read_i2c_block_data(self.slaveAdress,0,12)

        #Decodeing Raw byte array
        self.decoded_message = \
        {'Start_bytes'      :read_byte_array[0] *(2**8) + read_byte_array[1],  #0xBEAF
         'Buttonstate_bytes':read_byte_array[2] *(2**8) + read_byte_array[3],
         'Endoderpos_byte'  :read_byte_array[4]                        ,
         'Knob1pos_bytes'   :read_byte_array[5] *(2**8) + read_byte_array[6],
         'Knob2pos_bytes'   :read_byte_array[7] *(2**8) + read_byte_array[8],
         'Knob3pos_bytes'   :read_byte_array[9] *(2**8) + read_byte_array[10],
         'Source_byte'      :read_byte_array[11]
        }

        self.debugLogger.log("--------------------Recieved--------------------")


        self.debugLogger.log("Start_bytes:        Hex: " +   "{0:#0{1}x}".format(self.decoded_message['Start_bytes'],6)
                                               +" Decimal: "+(str(self.decoded_message['Start_bytes'])))
        self.debugLogger.log("Buttonstates_bytes: Hex: " +   "{0:#0{1}x}".format(self.decoded_message['Buttonstate_bytes'],6)
                                               +" Decimal: "+(str(self.decoded_message['Buttonstate_bytes'])))
        self.debugLogger.log("Encoder_pos_byte:   Hex: " +   "{0:#0{1}x}".format(self.decoded_message['Endoderpos_byte'],6)
                                               +" Decimal: "+(str(self.decoded_message['Endoderpos_byte'])))
        self.debugLogger.log("Knob1_bytes:        Hex: " +   "{0:#0{1}x}".format(self.decoded_message['Knob1pos_bytes'],6)
                                               +" Decimal: "+(str(self.decoded_message['Knob1pos_bytes'])))
        self.debugLogger.log("Knob2_bytes:        Hex: " +   "{0:#0{1}x}".format(self.decoded_message['Knob2pos_bytes'],6)
                                               +" Decimal: "+(str(self.decoded_message['Knob2pos_bytes'])))
        self.debugLogger.log("Knob3_bytes:        Hex: " +   "{0:#0{1}x}".format(self.decoded_message['Knob3pos_bytes'],6)
                                               +" Decimal: "+(str(self.decoded_message['Knob3pos_bytes'])))
        self.debugLogger.log("Source_bytes:       Hex: " +   "{0:#0{1}x}".format(self.decoded_message['Source_byte'],6)
                                               +" Decimal: "+(str(self.decoded_message['Source_byte'])))

        self.debugLogger.log("-----------------------End-----------------------")



    def evaluate_ui(self):
        """
        This function determines which radio controlls have changed -> which actions follow
        by parsing source byte and comparing with old mesagedy
        """

        self.debugLogger.log("> Entered action parser")

        #evaluate "changes" (in form of initial radio state for buttons)
        if (self.first_message_flag==True):
            self.debugLogger.log(">> Entered initial radio settup routine due to first package")
            
            self.__update_vol_level(mute=False)
            self.__update_eq_level()
            
            
            if  (self.decoded_message["Buttonstate_bytes"] == 1*(2**7)):
                self.debugLogger.log(">>> Button 1 (Mute) is pressed")
                self.mute_unmute_btn_aktive = 1
                self.__update_vol_level(mute=True)

            elif(self.decoded_message["Buttonstate_bytes"] == 1*(2**0)):
                self.debugLogger.log(">>> Button 8 (Unmute) is pressed")
                self.mute_unmute_btn_aktive = 0


            if(self.decoded_message["Buttonstate_bytes"] == 1*(2**6)):
                self.debugLogger.log(">>> Button 2 is pressed")
                self.ch_preset_btn = 0

            elif(self.decoded_message["Buttonstate_bytes"] == 1*(2**5)):
                self.debugLogger.log(">>> Button 3 is pressed")
                self.ch_preset_btn = 1
                
            elif(self.decoded_message["Buttonstate_bytes"] == 1*(2**4)):
                self.debugLogger.log(">>> Button 4 is pressed")
                self.ch_preset_btn = 2

            elif(self.decoded_message["Buttonstate_bytes"] == 1*(2**3)):
                self.debugLogger.log(">>> Button 5 is pressed")
                self.ch_preset_btn = 3

            elif(self.decoded_message["Buttonstate_bytes"] == 1*(2**2)):
                self.debugLogger.log(">>> Button 6 is pressed")
                self.ch_preset_btn = 4

            else:
                self.debugLogger.log(">>> No channel preset button is pressed, therefore selected nr.0")
                #choose arbituraty default
                self.ch_preset_btn = 0


            if(self.decoded_message["Buttonstate_bytes"] == 1*(2**1)):
                self.debugLogger.log(">>> Button 7 (Shutdown) is pressed")


            self.first_message_flag = False

        #Due to serial input of single change radio controll elements only one change is registered at a time
        else:
            self.debugLogger.log(">> Determining next action by source of change")
            if self.decoded_message["Source_byte"] == 0xFF:
                self.debugLogger.log(">>> Shutdown command recieved")
                self.change_src = "shutdown"

            elif self.decoded_message["Source_byte"] == 0x01:
                #self.debugLogger.log(">>> Button values have changed")
                #button_byte_diff = self.last_decoded_message["Buttonstate_bytes"] ^ self.decoded_message["Buttonstate_bytes"]
                button_byte = self.last_decoded_message["Buttonstate_bytes"]
                
                if  ( (button_byte & 1*(2**7))== 1*(2**7)):
                    self.debugLogger.log(">>> Button 1 has changed")
                    self.change_src = "btn_mute"
                    self.mute_unmute_btn_aktive = 1
                    self.volume = 0

                elif( (button_byte & 1*(2**6)) == 1*(2**6)):
                    self.debugLogger.log(">>> Button 2 has changed")
                    self.change_src = "btn_ch_pre_0"
                    self.ch_preset_btn = 0

                elif( (button_byte & 1*(2**5)) == 1*(2**5)):
                    self.debugLogger.log(">>> Button 3 has changed")
                    self.change_src = "btn_ch_pre_1"
                    self.ch_preset_btn = 1

                elif( (button_byte & 1*(2**4)) == 1*(2**4)):
                    self.debugLogger.log(">>> Button 4 has changed")
                    self.change_src = "btn_ch_pre_2"
                    self.ch_preset_btn = 2

                elif( (button_byte & 1*(2**3)) == 1*(2**3)):
                    self.debugLogger.log(">>> Button 5 has changed")
                    self.change_src = "btn_ch_pre_3"
                    self.ch_preset_btn = 3

                elif( (button_byte & 1*(2**2)) == 1*(2**2)):
                    self.debugLogger.log(">>> Button 6 has changed")
                    self.change_src = "btn_ch_pre_4"
                    self.ch_preset_btn = 4

                elif( (button_byte & 1*(2**1)) == 1*(2**1)):
                    self.debugLogger.log(">>> Button 7 has changed")
                    self.change_src = "btn_on_off"

                elif( (button_byte & 1*(2**0)) == 1*(2**0)):
                    self.debugLogger.log(">>> Button 8 has changed")
                    self.change_src = "unmute"
                    self.mute_unmute_btn_aktive = 0
                    self.__update_vol_level(mute=False)

                elif(button_byte == 0):
                    self.debugLogger.log(">>> No Button is selected")
                    self.change_src = "no_btn"
 

            elif self.decoded_message["Source_byte"] == 0x02:
                self.debugLogger.log(">>> Encoder position has changed")
                self.change_src = "enc_ch"
                self.ch_delta_enc = self.last_decoded_message["Endoderpos_byte"] - self.decoded_message["Endoderpos_byte"]
                

            elif self.decoded_message["Source_byte"] == 0x03:
                #self.debugLogger.log(">>> Knob values have changed")

                if   self.decoded_message["Knob1pos_bytes"] != self.last_decoded_message["Knob1pos_bytes"] \
                and abs(self.decoded_message["Knob1pos_bytes"] - self.last_decoded_message["Knob1pos_bytes"]) >POTI_FLUCT:
                    self.debugLogger.log(">>> Knob1 value has changed")
                    self.change_src = "knob1_vol"
                    self.__update_vol_level(mute=False)

                elif self.decoded_message["Knob2pos_bytes"] != self.last_decoded_message["Knob2pos_bytes"] \
                and abs(self.decoded_message["Knob2pos_bytes"] - self.last_decoded_message["Knob2pos_bytes"]) >POTI_FLUCT:
                    self.debugLogger.log(">>> Knob2 value has changed")
                    self.change_src = "knob2_high"
                    self.__update_eq_level()

                elif self.decoded_message["Knob3pos_bytes"] != self.last_decoded_message["Knob3pos_bytes"] \
                and abs(self.decoded_message["Knob3pos_bytes"] - self.last_decoded_message["Knob3pos_bytes"]) >POTI_FLUCT:
                    self.debugLogger.log(">>> Knob3 value has changed")
                    self.change_src = "knob3_low"
                    self.__update_eq_level()


        self.last_decoded_message = self.decoded_message
