import logging
class DebugLogger:
    def __init__(self,log_filename, log_to_con_flag,log_to_file_flag):
        self.logger = logging.getLogger("riio_logger")
        self.log_filename = log_filename
        self.log_to_con = log_to_con_flag
        self.log_to_file = log_to_file_flag

        if self.log_to_file:
            logging.basicConfig(filename=self.log_filename, filemode='w', level=logging.INFO,
                                format='%(asctime)s %(message)s',datefmt='%m/%d/%Y %I:%M:%S %p')

    # Wrapper for printing to stdout and logging to file
    def log(self,*params):
        if self.log_to_con== True:
            inp_strings = []
            for string in params:
                inp_strings.append(str(string))
                
            joined_str = "".join(inp_strings)
            print(joined_str)

        if self.log_to_file == True:
            logging.info(joined_str)