from smbus2 import SMBus
import warnings

import RPi.GPIO as GPIO
from subprocess import call
from datetime import datetime

from debug_logger import DebugLogger
from baicon_control import BaiconControl
from audio_control import AudioControl


try:
    """
    Import of steuermann only works if Musiko.py and Visio.py module is already running!!!
    """
    from steuermann import Musiko, Visio
    interprc_com_flag = True
except:
    interprc_com_flag = False


"""======================================================
                    SLAVE ADDRESSES
======================================================"""

BAICON_SLA_ADDR = 0x12
AUDIO_SLA_ADDR_PD = 0x54 #external pulldown
AUDIO_SLA_ADDR_PU = 0x2B #external pullup

"""======================================================
                    PATHS
======================================================"""
RASP_DEBUG_DIR = "/home/pi/retroradio-raspby/debug"
RASP_DEBUG_RIIO_LOG = RASP_DEBUG_DIR + "/riio.log"

class RiioSession:

    def __init__(self):
        self.i2cBus = SMBus(1)            # 1 indicates /dev/i2c-1 rpi device file*
        self.debugLogger = DebugLogger(RASP_DEBUG_RIIO_LOG,log_to_file_flag =True, log_to_con_flag = True)
        #self.debugLogger = DebugLogger(RASP_DEBUG_RIIO_LOG, log_to_con_flag = False)
        #self.debugLogger = DebugLogger(RASP_DEBUG_RIIO_LOG, log_to_con_flag = True)
        #self.debugLogger = None
        self.baiconControl = BaiconControl(self.debugLogger, self.i2cBus, BAICON_SLA_ADDR)
        self.audioControl = AudioControl(self.debugLogger, self.i2cBus, AUDIO_SLA_ADDR_PU)
        if(interprc_com_flag == True):
            self.radioCaptain = Musiko()
            self.displayCaptain = Visio()
        else:
            self.debugLogger.log("WARNING: Interprocess comunication will not work\n"+
             "DBus services not running.\n"+
             "Please make sure to run Musiko.py and Visio.py Python module\n"+
             "before excecuting riio_session.py")
            
        self.init_gpio()
        self.init_update_rq_eventhandler()



    def init_gpio(self):
        """This function initialises all GPIO pins"""
        GPIO.setmode(GPIO.BCM)              # set board mode to Broadcom
        self.baiconControl.init_gpio()
        self.audioControl.init_gpio()


    def init_update_rq_eventhandler(self):
        """This function sets up event handler"""
        if self.debuglogger is not None: 
        self.debugLogger.log("> Setting up eventhandler")
         
        #"""TEMPORARY"""
        #input_pin_EXIT = 25                         #GPIO 25 (pin 18: 11down from upper_right_corner)   
        #GPIO.remove_event_detect(input_pin_EXIT)    #just to get sure
        #GPIO.setup(input_pin_EXIT, GPIO.IN, pull_up_down=GPIO.PUD_UP)  
        #GPIO.add_event_detect(input_pin_EXIT, GPIO.FALLING,callback=self.exit_on_pinchange_handler,bouncetime=300)
        #"""TEMPORARY"""
        
        # Prevent I/O error in case pin change detection is already set
        GPIO.remove_event_detect(self.baiconControl.update_rq_pin)    
        # Register falling event detection to callback function
        GPIO.add_event_detect(self.baiconControl.update_rq_pin,GPIO.FALLING,callback = self.updateRQEventHandler,bouncetime=300)


    def updateRQEventHandler(self,update_rq_pin):
        """
        This function handles the main events triggered by changing radio controll
        elements (it MUST have the pin_nr as parameter or else it will not work!!!)
        """

        now = datetime.now()
        time_stamp = ('%02d:%02d:%02d.%d'%(now.hour,now.minute,now.second,now.microsecond))[:-4]
        self.debugLogger.log("\n",time_stamp," detected falling edge on GPIO: ",update_rq_pin)
        

        self.baiconControl.get_ui()
        msg_flag = self.baiconControl.get_message_flag()
        self.baiconControl.evaluate_ui()   
        change_src = self.baiconControl.get_source()
        
        if msg_flag == True: #initial radio settup
        
            init_volume = self.baiconControl.get_volume()
            init_eq = self.baiconControl.get_eq()
            self.audioControl.start(init_volume,init_eq)

            init_ch_preset_btn = self.baiconControl.get_ch_preset_btn()
            if(interprc_com_flag == True):
                self.radioCaptain.wechsel(init_ch_preset_btn)
                self.debugLogger.log(">>>> Setting initial radio channel to preset nr.: ",init_ch_preset_btn)
            
        else:
            if change_src == "shutdown":
                self.shutdown_routine()

            elif change_src in ["btn_mute","btn_unmute","knob1_vol"]:

                volume_int = self.baiconControl.get_volume()
                volume_float = volume_int /100
                
                mute_state = self.baiconControl.get_mute_state()
                if change_src == "btn_mute":
                    self.debugLogger.log(">>>> Muting Volume")
                    ###self.audioControl.change_volume_mute()
                    if(interprc_com_flag == True):
                        self.displayCaptain.set_volume(0)

                elif change_src == "btn_unmute":
                    self.debugLogger.log(">>>> Unmuting Volume back to: ",volume_int)
                    ###self.audioControl.change_volume_unmute(volume_float)
                    if(interprc_com_flag == True):
                        self.displayCaptain.set_volume(volume_int)

                elif change_src == "knob1_vol" and mute_state == 0:
                    self.debugLogger.log(">>>> Changing Volume to: ",volume_int)
                    ###self.audioControl.change_volume_unmute(volume_float)
                    if(interprc_com_flag == True):
                        self.displayCaptain.set_volume(volume_int)

            elif change_src in ["knob2_high", "knob3_low"]:
                eq = self.baiconControl.get_eq()
                ###self.audioControl.change_eq_settings(eq)         # eq = {"LOW" : value , "HIGH" : value} => both values in percent

            elif change_src in ["btn_ch_pre_0","btn_ch_pre_1","btn_ch_pre_2","btn_ch_pre_3","btn_ch_pre_4"]:
                ch_preset_btn = self.baiconControl.get_ch_preset_btn()
                if(interprc_com_flag == True):
                    self.radioCaptain.wechsel(ch_preset_btn)

            elif change_src == "enc_ch":
                ch_delta = self.baiconControl.get_ch_delta_enc()
                if(interprc_com_flag == True):
                    self.debugLogger.log(">>>> Advance played channel ",ch_delta," steps")
                    self.radioCaptain.delta(ch_delta)


    def shutdown_routine(self):
        """Control shutdown procedure of the RPI"""
                    
        self.debugLogger.log("> Stopping python radio module")
        if(interprc_com_flag == True):
            self.radioCaptain.stop()
                    
        self.debugLogger.log("> Shutting down amplifier")
        self.audioControl.stop(hard_shutdown=True)
        
        self.debugLogger.log("> Shutting down RPI")
        call("sudo poweroff",shell=True)    #Power down rpi
        
    
    #def exit_procedure_handler(self):
    #   """
    #    This function hanles the exit procedures and will clean
    #    up GPIO configurations
    #    
    #    Args:
    #        None
    #    Returns:
    #        None
    #    """
    #    
    #    GPIO.remove_event_detect(23)
    #    GPIO.remove_event_detect(25)
    #    GPIO.cleanup()
    #    
    #    self.debugLogger.log('Application exit')
    
     
    #Temporary
    #def exit_on_pinchange_handler(self,pin):
    #    now = datetime.now()
    #    time_stamp = ('%02d:%02d:%02d.%d'%(now.hour,now.minute,now.second,now.microsecond))[:-4]
    #    
    #    print (time_stamp,"detected falling edge on GPIO:",pin,"\n")
    #    
    #    self.exit_procedure_handler()
    #    exit()
        



def main():
    riio = RiioSession()

    while(1):
        pass


if __name__ == "__main__":
    main()

