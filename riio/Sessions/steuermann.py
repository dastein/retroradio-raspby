import dbus

dbus_exp = dbus.exceptions.DBusException


class Musiko:
    """
    This Class provides DBUS-Interface-Methods to controll Musiko, the Radio's
    Soundserver
    """

    dbus_service = 'hm.retro.Musiko'
    dbus_object = '/hm/retro/Musiko'
    hipster_M = dbus.SessionBus().get_object(dbus_service, dbus_object)

    def __init__(self):
        self.radio_an = False
        self.diff = 0

    def start(self, tastennr):
        if not self.radio_an:
            try:
                # when setting the preset you give the key code directly,
                # otherwise hipster.start... is called by the delta-method,
                # which sets the variables so musiko realizes delta_mode is
                # required
                self.hipster_M.start_radio(tastennr, self.diff)
                self.radio_an = True
            except dbus_exp as e:
                print(e)
                self.radio_an = False
        else:
            print("Radio bereits gestartet!")

    def stop(self):
        if self.radio_an:
            try:
                self.hipster_M.stop_radio()
            except dbus_exp as e:
                print(e)
        else:
            print("Radio bereits gestoppt!")
        self.radio_an = False

    # recommended to use, but not mandatory of course
    def wechsel(self, tastennr):
        self.stop()
        self.start(tastennr)

    # Spielt den zuletzt gewählten Sender. Default = index 0
    def restart(self):
        self.start(-2)

    # tells musiko that you want to play the sender with current_index+delta
    # delta hast to be an integer.
    # musiko realizes you want to use delta_mode if it gets a negative key-code
    # and a delta != 0
    def delta(self, x):
        self.diff = x
        self.stop()
        self.start(-1)
        self.diff = 0


class Visio:
    dbus_service = 'hm.retro.Visio'
    dbus_object = '/hm/retro/Visio'
