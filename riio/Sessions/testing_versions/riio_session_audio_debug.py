from smbus2 import SMBus

import RPi.GPIO as GPIO
import time
from subprocess import call
from datetime import datetime

from debug_logger import DebugLogger
from baicon_control import BaiconControl
from audio_control import AudioControl

"""
Import of steuermann only works if Musiko.py module is already Running!!!
"""
###from steuermann import Musiko


BAICON_SLA_ADDR = 0x12
AUDIO_SLA_ADDR_PD = 0x54 #external pulldown
AUDIO_SLA_ADDR_PU = 0x2B #external pullup

class RiioSession:

    def __init__(self):
        self.i2cBus = SMBus(1)              # 1 indicates /dev/i2c-1 rpi device file*
        self.debugLogger = DebugLogger("riio.log",True)
        self.baiconControl = BaiconControl(self.debugLogger, self.i2cBus, BAICON_SLA_ADDR)
        self.audioControl = AudioControl(self.debugLogger, self.i2cBus, AUDIO_SLA_ADDR_PU)
        ###self.radioCaptain = Musiko()
        self.init_gpio()
        self.init_update_rq_eventhandler()

    def start(self):
        #get_ui for the first time here and do all intial settings
        self.audioControl.start(0.5)

    def stop(self, hard_shtdwn = False):
        self.audioControl.stop(hard_shtdwn)

    def init_gpio(self):
        """This function initialises all GPIO Pins"""
        GPIO.setmode(GPIO.BCM)              # set board mode to Broadcom
        self.baiconControl.init_gpio()
        self.audioControl.init_gpio()


    def init_update_rq_eventhandler(self):
        """This function sets up event handler"""
        self.debugLogger.log("> Setting up eventhandler")
        
        
        """TEMPORARY"""
        #GPIO 25 (pin 18: 11down from upper_right_corner)
        input_pin_EXIT = 25   
        # Set up PIN as input, pulled up to avoid false detection.    
        # Set falling edge detection for PIN

        GPIO.remove_event_detect(input_pin_EXIT)    # Prevent I/O error in case pin change detection is already set
        GPIO.remove_event_detect(self.baiconControl.updateRqPin)    # Prevent I/O error in case pin change detection is already set

        GPIO.setup(input_pin_EXIT, GPIO.IN, pull_up_down=GPIO.PUD_UP)  
        #ADD FALLING EDGE EVENT HANDLER
        GPIO.add_event_detect(input_pin_EXIT, GPIO.FALLING,callback=self.exit_on_pinchange_handler,bouncetime=300) 
        """TEMPORARY"""
        
        # Register falling event detection to callback function
        GPIO.add_event_detect(self.baiconControl.updateRqPin, 
                              GPIO.FALLING,
                              callback = self.updateRQEventHandler,
                              bouncetime=300)

    def updateRQEventHandler(self,update_rq_pin):
        """
        This function handles the main events triggered by changing radio controll
        elements (it MUST have the pin_nr as parameter or else it will not work!!!)
        """
        now = datetime.now()
        time_stamp = ('%02d:%02d:%02d.%d'%(now.hour,now.minute,now.second,now.microsecond))[:-4]
        self.debugLogger.log("\n",time_stamp," detected falling edge on GPIO: ",update_rq_pin)

        self.baiconControl.get_ui()                         #calls read_i2c_block <Tim>
        msg_flag = self.baiconControl.get_message_flag()
        self.baiconControl.evaluate_ui()
        change_src = self.baiconControl.get_source()

        if msg_flag == True: #initial radio settup
        
            volume = self.baiconControl.get_volume()
            ####self.audioStream.change_volume_settings(volume)

            eq = self.baiconControl.get_eq()
            ####self.audioStream.change_eq_settings(eq)

            ch_preset_btn = self.baiconControl.get_ch_preset_btn()
            self.debugLogger.log(">>>> Setting initial Channel: ",ch_preset_btn)
            self.radioCaptain.wechsel(ch_preset_btn)
        else:
            if change_src == "shutdown":
                self.shutdown_routine()

            elif change_src in ["btn_mute","btn_unmute","knob1_vol"]:

                #Needs state machine if mute or unmute btn is currently pressed
                volume = self.baiconControl.get_volume()
                mute_state = self.baiconControl.get_mute_stae()
                if change_src == "btn_mute":
                    self.audioStream.change_volume_mute()

                elif change_src == "btn_unmute":
                    self.audioStream.change_volume_unmute(volume)

                elif change_src == "knob1_vol" and mute_state == 0:
                    self.audioStream.change_volume_unmute(volume)



            elif change_src in ["knob2_high", "knob3_low"]:
                eq = self.baiconControl.get_eq()
                self.audioStream.change_eq_settings(eq)         # eq = {"LOW" : value , "HIGH" : value} => both values in percent

            elif change_src in ["btn_ch_pre_1","btn_ch_pre_2","btn_ch_pre_3","btn_ch_pre_4","btn_ch_pre_5"]:
                ch_preset_btn = self.baiconControl.get_ch_preset_btn()
                ###self.radioCaptain.wechsel(ch_preset_btn)

            elif change_src == "enc_ch":
                ch_delta_enc = self.baiconControl.get_ch_delta_enc()
                ###self.radioCaptain.delta(ch_delta_enc)


    def shutdown_routine(self):
        """Control shutdown procedure of the RPI"""
        
        self.debugLogger.log("> Stopping python radio module")
        self.radioCaptain.stop()
        
        self.debugLogger.log("> Shutting down amplifier")
        self.audioControl.quick_shutdown()
        
        self.debugLogger.log("> Shutting down RPI")
        call("sudo poweroff",shell=True)    #Power down rpi
        
    
    def exit_procedure_handler(self):
        """
        This function hanles the exit procedures and will clean
        up GPIO configurations
        
        Args:
            None
        Returns:
            None
        """
        
        GPIO.remove_event_detect(23)
        GPIO.remove_event_detect(25)
        GPIO.cleanup()
        
        self.debugLogger.log('Application exit')
    
     
    #Temporary
    def exit_on_pinchange_handler(self,pin):
        now = datetime.now()
        time_stamp = ('%02d:%02d:%02d.%d'%(now.hour,now.minute,now.second,now.microsecond))[:-4]
        
        print (time_stamp,"detected falling edge on GPIO:",pin,"\n")
        
        self.exit_procedure_handler()
        exit()




def main():
    riio = RiioSession()
    riio.start()
    for t in range(5,11):
        riio.debugLogger.log("> Volume in percent: %f", t/10.0)
        riio.audioControl.change_volume_settings(t/10.0)
        time.sleep(5)
    time.sleep(3000)
    riio.stop()

    #while(1):
    #    pass


if __name__ == "__main__":
    main()
