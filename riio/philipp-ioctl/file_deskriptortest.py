from fcntl import ioctl

pfad = "/dev/i2c-1"
slave_addr = 0x12
READ_AMOUNT = 4

deskriptor = open(pfad, "rb")

ioctl(deskriptor, 0x0703, slave_addr)

daten = deskriptor.read(READ_AMOUNT)
deskriptor.close()

print(daten)
