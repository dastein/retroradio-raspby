#include <linux/i2c-dev.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <stdbool.h>

#define BUFFSIZE 20
#define TO_READ 4

int main(int argc, char *argv[])
{
	int file;
	int adapter_nr = 1;
	char filename[20];

	/* 0x24 entspricht im Register (7+1 Bit) Adresse 0x12 */
	int addr = 0x12;
	uint8_t daten[BUFFSIZE] = {0};

	if (argc > 1)
		addr = atoi(argv[1]);


	snprintf(filename, 19, "/dev/i2c-%d", adapter_nr);

	file = open(filename, O_RDWR);

	if (file < 0) {
		printf("Fehler: Oeffnen der Datei nicht moeglich!\n");
		exit(1);
	}

	printf("Adresse: %02x\n", addr);
	if (ioctl(file, I2C_SLAVE, addr) < 0) {
		printf("Problem mit der Adresse!\n");
		exit(1);
	}

	if (read(file, daten, TO_READ) < TO_READ)
		printf("Weniger als 4 Byte gelesen!\n");

	for (int i=0; i<4; i++)
		printf("Element %i: %02x\n", i, daten[i]);


	close(file);

	return 0;
}
