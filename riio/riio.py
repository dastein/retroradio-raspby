import time
from datetime import datetime
from subprocess import call
import logging

import json
import os

import dbus
import sys

import RPi.GPIO as GPIO
import smbus


#Global variables
message_count = 0
potentiometer_fluctuation = 100
last_decoded_message = {'Start_bytes':0x0000,
                       'Buttonstate_bytes':0x0000,
                       'Endoderpos_byte':0x00,
                       'Knob1pos_bytes':0x0000,
                       'Knob2pos_bytes':0x0000,
                       'Knob3pos_bytes':0x0000,
                       'Source_byte':0x00}

if (os.path.isfile(os.path.abspath("riio.json"))):
    with open ("riio.json") as json_file:
        settings_at_exit = json.load(json_file)
    json_file.close()
else:
    settings_at_exit = last_decoded_message

exp = dbus.exceptions.DBusException

logger = logging.getLogger("riio_logger")
logging.basicConfig(filename='riio.log',filemode='w',level=logging.INFO,format='%(asctime)s %(message)s',datefmt='%m/%d/%Y %I:%M:%S %p')
debug_flag = True

def main(): 
    my_log_print("Application Starting",debug_flag)
    
    #Setting up interprocess comunikation over dbus
    """
    bus = dbus.SessionBus()
    musikio_bus_obj = bus.get_object('hm.retro.Musiko', '/hm/retro/Musiko')
    radio_an = False
    """
    
    #Set all GPIO Pin functionalities
    set_gpio_configuration()

    
    while(1):
        #Delay is somehow necessary to view complete printout
        time.sleep(10)
        
def set_gpio_configuration():
    """
    This function configures all GPIO PIN functionalities
    
    Args:
        None
    Returns:
        None
    """
    # set board mode to Broadcom
    GPIO.setmode(GPIO.BCM)
    
    ###Setting GPIO PIN Configurations
    
    ##POWERSTATE_PIN
    #GPIO 24 (pin 16: 9down from upper_right_corner)
    output_pin_PIPOWERSTATE = 24
    # Set PIPOWERSTATE PIN to output
    GPIO.setup(output_pin_PIPOWERSTATE,GPIO.OUT)
    # Set PIPOWERSTATE PIN output to high
    GPIO.output(output_pin_PIPOWERSTATE,1)
    
    ##PINCHANGE_PIN
    #GPIO 23 (pin 18: 8down from upper_right_corner)
    input_pin_PINCHANGE = 23     
    # Set up PIN as input, pulled up to avoid false detection.    
    # Set falling edge detection for PIN
    GPIO.setup(input_pin_PINCHANGE, GPIO.IN, pull_up_down=GPIO.PUD_UP)  
    #ADD FALLING EDGE EVENT HANDLER 
    #GPIO.add_event_detect(channel, GPIO.FALLING, callback=lambda x,y : akt_on_radio_button_event_handler(channel,i2c_bus,i2c_slave_adress), bouncetime=300) 
    GPIO.add_event_detect(input_pin_PINCHANGE, GPIO.FALLING,callback=radio_button_changed_event_handler,bouncetime=300) 
    
    
    ##TEMPORARY EXIT BUTTON PIN CONFIGURATION
    #GPIO 25 (pin 18: 11down from upper_right_corner)
    input_pin_EXIT = 25   
    # Set up PIN as input, pulled up to avoid false detection.    
    # Set falling edge detection for PIN
    GPIO.setup(input_pin_EXIT, GPIO.IN, pull_up_down=GPIO.PUD_UP)  
    #ADD FALLING EDGE EVENT HANDLER
    GPIO.add_event_detect(input_pin_EXIT, GPIO.FALLING,callback=exit_on_pinchange_handler,bouncetime=300) 

def radio_button_changed_event_handler(input_pin_PINCHANGE):
    """
    This is a callback funtion for an event
    which will run in another thread when our event is detected
    It will akt on GPIO Pinchange from (High -> Low)
    &
    execute next procedures based on which values have changed
    
    Args:
        pin_nr = int value for gpio pin where event detetction has been added
    Returns:
        None
    """
    global message_count
    global last_decoded_message 
    global settings_at_exit
    
    now = datetime.now()
    time_stamp = ('%02d:%02d:%02d.%d'%(now.hour,now.minute,now.second,now.microsecond))[:-4]
    my_log_print (time_stamp+" detected falling edge on GPIO: "+str(input_pin_PINCHANGE),debug_flag)
    
    #Read raw 12 byte array
    raw_byte_array = initiate_i2c_communication_read_bytes()
    
    message_count += 1
    my_log_print("Total Byte blocks recieved: "+str(message_count),debug_flag)
    
    #Decode the byte array
    decoded_message = decode_byte_array(raw_byte_array)
    
    #Check if setup package (first package) has arrived
    if(message_count == 1):
        #change radio station to knob_value for station select
        #change volume level to Knov_vlaue for volume level
        pass
    else:
        determine_action(decoded_message,last_decoded_message)
    
    my_log_print("\n",debug_flag)
    
    
    #Updated last recieved values
    last_decoded_message = decoded_message
    settings_at_exit = last_decoded_message

def initiate_i2c_communication_read_bytes():
    """
    This function will initiate the i2c multibyte read communikation
    and return raw read data
    
    Args:
        None
    Returns:
        read_byte_array = raw byte array
    """
    
    # 0 indicates /dev/i2c-0-+*
    i2c_bus = smbus.SMBus(1)
    #Set slave adress
    i2c_slave_adress = 0x12

    my_log_print("Trying to read data block from slave transmitter",debug_flag)
    read_byte_array = i2c_bus.read_i2c_block_data(i2c_slave_adress,message_count,12)
    
    
    return read_byte_array

def decode_byte_array(byte_array):
    """
    This function parses and decodes the raw byte array
    
    Args:
        byte_array = raw byte array
    Returns:
        decoded_byte_array = byte array decoded into dict
    """
    
    #Return dict (12 byte message)
    decoded_byte_array = { 'Start_bytes'      :byte_array[0] *(2**8) + byte_array[1],  #0xBEAF
                           'Buttonstate_bytes':byte_array[2] *(2**8) + byte_array[3],
                           'Endoderpos_byte'  :byte_array[4]                        ,
                           'Knob1pos_bytes'   :byte_array[5] *(2**8) + byte_array[6],
                           'Knob2pos_bytes'   :byte_array[7] *(2**8) + byte_array[8],
                           'Knob3pos_bytes'   :byte_array[9] *(2**8) + byte_array[10],
                           'Source_byte'      :byte_array[11] 
                           }
    
    my_log_print("--------------------Recieved--------------------",debug_flag)
    
    
    my_log_print("Start_bytes:        Hex: " +   "{0:#0{1}x}".format(decoded_byte_array['Start_bytes'],6)
                                   +" Decimal: "+(str(decoded_byte_array['Start_bytes'])),debug_flag) 
    my_log_print("Buttonstates_bytes: Hex: " +   "{0:#0{1}x}".format(decoded_byte_array['Buttonstate_bytes'],6) 
                                   +" Decimal: "+(str(decoded_byte_array['Buttonstate_bytes'])),debug_flag)
    my_log_print("Encoder_pos_byte:   Hex: " +   "{0:#0{1}x}".format(decoded_byte_array['Endoderpos_byte'],6)   
                                   +" Decimal: "+(str(decoded_byte_array['Endoderpos_byte'])),debug_flag) 
    my_log_print("Knob1_bytes:        Hex: " +   "{0:#0{1}x}".format(decoded_byte_array['Knob1pos_bytes'],6)    
                                   +" Decimal: "+(str(decoded_byte_array['Knob1pos_bytes'])),debug_flag)
    my_log_print("Knob2_bytes:        Hex: " +   "{0:#0{1}x}".format(decoded_byte_array['Knob2pos_bytes'],6)    
                                   +" Decimal: "+(str(decoded_byte_array['Knob2pos_bytes'])),debug_flag)
    my_log_print("Knob3_bytes:        Hex: " +   "{0:#0{1}x}".format(decoded_byte_array['Knob3pos_bytes'],6)    
                                   +" Decimal: "+(str(decoded_byte_array['Knob3pos_bytes'])),debug_flag)
    my_log_print("Source_bytes:       Hex: " +   "{0:#0{1}x}".format(decoded_byte_array['Source_byte'],6)       
                                   +" Decimal: "+(str(decoded_byte_array['Source_byte'])),debug_flag) 
    
    my_log_print("-----------------------End-----------------------",debug_flag)
    
    return decoded_byte_array

def determine_action(new_message,old_message):
    """
    This function determines which radio controlls have changed -> which actions follow
    by reading source byte and comparing with old mesagedy
    
    Args:
        new_messsage = recently read decoded byte array
        old_messaage = last read decoded byte array
    Returns:
        None

    """
   
    my_log_print("Entered action parser",debug_flag)
    

    if new_message["Source_byte"] == 0xFF:
        my_log_print("Shutdown command recieved",debug_flag)
        shuttdown_pi()
        
    elif new_message["Source_byte"] == 0x01:
        my_log_print("Button values have changed",debug_flag)
        
        if  (new_message["Buttonstate_bytes"] == 1*(2**7)):
            my_log_print("Button 1 has changed",debug_flag)
            
        elif(new_message["Buttonstate_bytes"] == 1*(2**6)):
            my_log_print("Button 2 has changed",debug_flag)
            
        elif(new_message["Buttonstate_bytes"] == 1*(2**5)):
            my_log_print("Button 3 has changed",debug_flag)
            
        elif(new_message["Buttonstate_bytes"] == 1*(2**4)):
            my_log_print("Button 4 has changed",debug_flag)
            
        elif(new_message["Buttonstate_bytes"] == 1*(2**3)):
            my_log_print("Button 5 has changed",debug_flag)
            
        elif(new_message["Buttonstate_bytes"] == 1*(2**2)):
            my_log_print("Button 6 has changed",debug_flag)
            
        elif(new_message["Buttonstate_bytes"] == 1*(2**1)):
            my_log_print("Button 7 has changed",debug_flag)
        
        elif(new_message["Buttonstate_bytes"] == 1*(2**0)):
            my_log_print("Button 8 has changed",debug_flag)
            
            
            
    
    elif new_message["Source_byte"] == 0x02:
        my_log_print("Encoder position has changed",debug_flag)
        
        #Channel has changed
        
    elif new_message["Source_byte"] == 0x03:
        my_log_print("Knob values have changed",debug_flag)
        
        if   new_message["Knob1pos_bytes"] != old_message["Knob1pos_bytes"] \
        and abs(new_message["Knob1pos_bytes"] - old_message["Knob1pos_bytes"]) > potentiometer_fluctuation:
            my_log_print("Knob1 value has changed",debug_flag)
            
        elif new_message["Knob2pos_bytes"] != old_message["Knob2pos_bytes"] \
        and abs(new_message["Knob2pos_bytes"] - old_message["Knob2pos_bytes"]) > potentiometer_fluctuation:
            my_log_print("Knob2 value has changed",debug_flag) 
            
        elif new_message["Knob3pos_bytes"] != old_message["Knob3pos_bytes"] \
        and abs(new_message["Knob3pos_bytes"] - old_message["Knob3pos_bytes"]) > potentiometer_fluctuation:
            my_log_print("Knob3 value has changed",debug_flag) 

            

## Functions called based on which digital value changed
def shuttdown_pi():
    """
    This function will shuttdown the RPi
    
    Args:
        None
    Returns:
        None
    """
    
    exit_procedure_handler()
    
    call("sudo poweroff",shell=True)  

            
def exit_procedure_handler():
    """
    This function hanles the exit procedures and will clean
    up GPIO configurations
    
    Args:
        None
    Returns:
        None
    """
    global settings_at_exit
    
    json_file_path = os.path.abspath("riio.json")
    with open (json_file_path,'w') as file:
        json.dump(settings_at_exit,file,indent=4,sort_keys=True)
    file.close()
    my_log_print("Stored last radio settings in rrio.json file",debug_flag)
    
    GPIO.remove_event_detect(23)
    GPIO.remove_event_detect(25)
    GPIO.cleanup()
    
    my_log_print ('Application exit',debug_flag)
    
#Wrapper for printing to stdout and logging to file
def my_log_print(string,print_enable=0):
    if(print_enable):
        print(string)
    logging.info(string)

    
#Temporary
def exit_on_pinchange_handler(pin):
    now = datetime.now()
    time_stamp = ('%02d:%02d:%02d.%d'%(now.hour,now.minute,now.second,now.microsecond))[:-4]
    
    print (time_stamp,"detected falling edge on GPIO:",pin,"\n")
    
    exit_procedure_handler()
    exit()
    

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
