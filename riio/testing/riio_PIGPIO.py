import time
import pigpio
from subprocess import call

def start_pigpio_deamon():
    call("sudo pigpiod",shell=True)
def stop_pigpio_deamon():
    call("sudo killall pigpiod",shell=True)
    
def shuttdown_pi():
    call("sudo poweroff",shell=True)

def main():
    #Start Pigpio Deamon
    #start_pigpio_deamon()
    
    #Initialize Pi connection
    pi = pigpio.pi()
    
    if not pi.connected:
        exit(0)
        
    #GPIO 24 (pin 16: 9down->upper_right_corner) s
    output_pin_PIPOWERSTATE = 24
    #Set as Output
    pi.set_mode(output_pin_PIPOWERSTATE,pigpio.OUTPUT)
    #Set to High
    pi.write(output_pin_PIPOWERSTATE,1)
    
    #Set GPIO PIN
    #GPIO 23 (pin 18: 8down->upper_right_corner) s
    input_pin_PINCHANGE = 23
    #Set as Input
    pi.set_mode(input_pin_PINCHANGE,pigpio.INPUT)
    #Set as pullup
    pi.set_pull_up_down(input_pin_PINCHANGE,pigpio.PUD_UP)
    #Set watchdog on input pin which is triggered by PINCHANGE
    pi.set_watchdog(input_pin_PINCHANGE,0)
        
    bus = 1
    p = pi.i2c_open(bus,0x48)
    while(1):
        
        time.sleep(1)
        anzahl,read_block = pi.i2c_read_i2c_block_data(p,0,14)
        #read_block = pi.i2c_read_block_data(p,14)
        print(read_block)
        
        
    """
    print("------Recieved-----")
    
    print("Start_bytes:       " ,   hex(read_block[0] *(2**8) + read_block[1]  ))
    print("Buttonstates_bytes:" ,   hex(read_block[2] *(2**8) + read_block[3]  ))
    print("Knob1_bytes:       " ,   hex(read_block[4] *(2**8) + read_block[5]  ))
    print("Knob2_bytes:       " ,   hex(read_block[6] *(2**8) + read_block[7]  ))
    print("Knob3_bytes:       " ,   hex(read_block[8] *(2**8) + read_block[9]  ))
    print("Knob4_bytes:       " ,   hex(read_block[10]*(2**8) + read_block[11] ))
    print("Shuttdown_bytes:   " ,   hex(read_block[12]*(2**8) + read_block[13] ))
    
    print("--------End--------")
    """
    
    pi.stop()
    
    #stop_pigpio_deamon()
    

main()