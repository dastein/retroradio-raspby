import time
from datetime import datetime
import atexit
from subprocess import call

from periphery import GPIO,I2C

def shuttdown_pi():
    call("sudo poweroff",shell=True)


def main():
	#GPIO 24 (pin 16: 9down->upper_right_corner)
    output_pin_PIPOWERSTATE = GPIO(24,"out")
    # set PIPOWERSTATE PIN output to high
    output_pin_PIPOWERSTATE.write(True)

    
    #GPIO 23 (pin 18: 8down->upper_right_corner)
    #!!!!MUST SET EXTERNAL PULLUP
    input_pin_PINCHANGE = GPIO(23,"in")

    #open i2c-1 controller
    i2c_bus = I2C("/dev/i2c-1")
    #i2c slave adress
    i2c_slave_adress = 0x48
    
    # set up PIN as input, pulled up to avoid false detection.  
    # Port wired to connect to GND (bottom_left_corn r)on button press.  
    # So we'll be setting up falling edge detection for pin 18
    input_pin_PINCHANGE.edge = "falling"
    
    print(input_pin_PINCHANGE.supports_interrupts)

    
    while(True):      
        #print("Waiting for falling Edge")
        if(input_pin_PINCHANGE.poll(1.5)==True):
            print("Falling edge occurred")
            
            #Read 14Bytes
            recieved_multibyte_block = [0]*14
                
            msg = [I2C.Message(recieved_multibyte_block,True)]
                
            print("initiate transfer protocall & recieve multibyte data package")
            try:
                i2c_bus.transfer(i2c_slave_adress,msg)
                
                print("------Recieved-----")
        
                print("Start_bytes:       " ,   hex(recieved_multibyte_block[0] *(2**8) + recieved_multibyte_block[1]  ))
                print("Buttonstates_bytes:" ,   hex(recieved_multibyte_block[2] *(2**8) + recieved_multibyte_block[3]  ))
                print("Knob1_bytes:       " ,   hex(recieved_multibyte_block[4] *(2**8) + recieved_multibyte_block[5]  ))
                print("Knob2_bytes:       " ,   hex(recieved_multibyte_block[6] *(2**8) + recieved_multibyte_block[7]  ))
                print("Knob3_bytes:       " ,   hex(recieved_multibyte_block[8] *(2**8) + recieved_multibyte_block[9]  ))
                print("Knob4_bytes:       " ,   hex(recieved_multibyte_block[10]*(2**8) + recieved_multibyte_block[11] ))
                print("Shuttdown_bytes:   " ,   hex(recieved_multibyte_block[12]*(2**8) + recieved_multibyte_block[13] ))
                
                print("--------End--------")
            
                #14th byte = shuttdown state
                shuttdown = recieved_multibyte_block[13]
                
                if(shuttdown == 0xFF):
                    print("Shuttdown Pi")
                    output_pin_PIPOWERSTATE.close()
                    input_pin_PINCHANGE.close()
                    break;
                
            except Exception as e:
                print(e)
                #break;
        else:
            #print("nope")
            pass
    
    
       
    

def exit_event_handler():
    print ('My application is ending!')
    #output_pin_PIPOWERSTATE.close()
    #input_pin_PINCHANGE.close()
    
main()