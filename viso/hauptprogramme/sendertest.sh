#!/bin/bash

for i in {0..99}
do
	busctl --system call hm.retro.Visio /hm/retro/Visio hm.retro.Visio new_index i $i
	sleep 0.25s
done
