//Console log auskommentieren nicht vergessen

const express = require('express');
var fs = require("fs");
const app = express();
const fileUpload = require('express-fileupload');
var path    = require("path");
var url = require('url');
var bodyParser = require('body-parser');
var fw = require('fs');//require('fs');	// stream to write the Config file
var exec = require('child_process').exec;
var child;



// 3 Möglichkeiten für configpath. Leider kann ich das nicht testen. 
// Falls das erste icht geht, so probiere es mal für die anderen 2 darunter

const config_path= '/home/pi/retroradio-raspby/config-dateien';

// const config_path= 'home/pi/retroradio-raspby/config-dateien';
// const config_path= __dirname + '/home/pi/retroradio-raspby/config-dateien';

// __dirname wenn über Gitlab ?????? Zum herausfinden vllt console log versuchen:
// console.log(__dirname);

app.use(bodyParser.urlencoded({
  extended: true
}))

app.use(bodyParser.json());

// einbinden der Bibliotheken für javascript und jquery; sind im Ordner /Js
app.use('/Js', express.static(__dirname + '/Js'));

// app.use(express.static('config_path'));

// app.use('/form', express.static(__dirname + '/MyWebradio.html'));

// Für den externen File upload (eigene JSON config) 
app.use(fileUpload());


// starts the application
app.get('/',function(req,res){
  res.sendFile(path.join(__dirname+'/MyWebradio.html'));
});

// Get Values from JSON-Files:
// 1.) read (default) Pool -> Welcher Pfad?

var pathp = require('path');
var filePathP = pathp.join(config_path, 'url_pool.json');
const Pool = fs.readFileSync(filePathP);
var jsPool = JSON.parse(Pool);

// 2.) read (default) Config

var pathc = require('path');
// Lese existierendes retroradio_config.json aus
var filePathC = pathc.join(config_path, 'retroradio_config.json');
const Config = fs.readFileSync(filePathC);
var jsConfig = JSON.parse(Config);

// console.log("Config: " + JSON.stringify(jsConfig));

// to starts the app, tipe : http://localhost:3000/ as URL



// sends the pool to the Client so that he can create the list
app.get('/pool', (req, res) => {
  res.send(jsPool);
});

// sends the config to the Client so that he can create the existing config list
app.get('/config', (req, res) => {
  res.send(jsConfig);
});


// Vom Client zum Webserver geschickt, sobald auf Upload Mylist geklickt wurde:
app.post('/config_new',  (req, res)  =>{
 
	// GETS the new JSON String from the Client and (re)write file
	//--------------------------- (Re)write the Config File and save ------------------------------
	
	fw.writeFile(config_path +'/retroradio_config.json', JSON.stringify(req.body.q), function(err, data){
		if (err)
		{
			console.log(err);
		} else {
		console.log("Successfully Written to File: JUCHUUUUU.");
		}
	});

	
	// BUSTCTL BEFEHLE:
	
	child = exec("busctl call --system hm.retro.Musiko /hm/retro/Musiko hm.retro.Musiko config_change", function (error, stdout, stderr) {
	console.log('stdout: ' + stdout);
	console.log('stderr: ' + stderr);
	if (error !== null) {
		console.log('exec error: ' + error);
	}
	});

	child = exec("busctl call --system hm.retro.Visio /hm/retro/Visio hm.retro.Visio config_change", function (error, stdout, stderr) {
	console.log('stdout: ' + stdout);
	console.log('stderr: ' + stderr);
	if (error !== null) {
		console.log('exec error: ' + error);
	}
	});
	
	// BUSTCTL BEFEHLE ENDE
}); 


// Upload externer, selbst erstellter Config-Datei (auf Wunsch von Hr. Schramm)
app.post('/upload', function(req, res) {
  let sampleFile;
  let uploadPath;

  if (Object.keys(req.files).length == 0) {
    res.status(400).send('No files were uploaded.');
    return;
  }

 // console.log('req.files >>>', req.files); // eslint-disable-line
  sampleFile = req.files.sampleFile;
 // uploadPath = __dirname + '/config_path/' + sampleFile.name;
 uploadPath = config_path + '/' + sampleFile.name;

  sampleFile.mv(uploadPath, function(err) {
    if (err) {
      return res.status(500).send(err);
    }
	
	// Reloads the Applikation
	return res.sendFile(path.join(__dirname +'/MyWebradio.html'));
  });
});


app.listen(3000, () => {
  console.log('Server started at http://localhost:3000/');
});
